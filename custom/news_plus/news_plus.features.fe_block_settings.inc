<?php
/**
 * @file
 * news_plus.features.fe_block_settings.inc
 */

/**
 * Implements hook_default_fe_block_settings().
 */
function news_plus_default_fe_block_settings() {
  $export = array();

  $export['version'] = '2.0';

  $export['views-news_teasers_page-block_2'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => 'news_teasers_page-block_2',
    'module' => 'views',
    'node_types' => array(),
    'pages' => '<front>
news/*',
    'roles' => array(),
    'themes' => array(
      'bartik' => array(
        'region' => 'sidebar_second',
        'status' => 1,
        'theme' => 'bartik',
        'weight' => -14,
      ),
      'seven' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'seven',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 1,
  );

  $export['views-recent_news_comments-block_1'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => 'recent_news_comments-block_1',
    'module' => 'views',
    'node_types' => array(),
    'pages' => 'news
news/*',
    'roles' => array(),
    'themes' => array(
      'bartik' => array(
        'region' => 'sidebar_second',
        'status' => 1,
        'theme' => 'bartik',
        'weight' => -11,
      ),
      'seven' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'seven',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 1,
  );

  return $export;
}
