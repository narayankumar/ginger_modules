<?php
/**
 * @file
 * news_plus.features.uuid_term.inc
 */

/**
 * Implements hook_uuid_features_default_terms().
 */
function news_plus_uuid_features_default_terms() {
  $terms = array();

  $terms[] = array(
    'name' => 'Animal Cruelty',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 0,
    'uuid' => '203d4b8a-7052-4374-a3c8-04c718683a07',
    'vocabulary_machine_name' => 'news_categories',
  );
  $terms[] = array(
    'name' => 'Inspirational Stories',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 0,
    'uuid' => 'e066d6a0-ec6e-461c-aa9f-696693ee4886',
    'vocabulary_machine_name' => 'news_categories',
  );
  return $terms;
}
