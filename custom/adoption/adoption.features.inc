<?php
/**
 * @file
 * adoption.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function adoption_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "context" && $api == "context") {
    return array("version" => "3");
  }
  if ($module == "page_manager" && $api == "pages_default") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function adoption_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_fe_nodequeue_export_fields().
 */
function adoption_fe_nodequeue_export_fields() {
  $nodequeues = array();

  // Exported nodequeues: ad_block_adoption
  $nodequeues['ad_block_adoption'] = array(
    'name' => 'ad_block_adoption',
    'title' => 'Ad block - Adoption',
    'subqueue_title' => '',
    'size' => 4,
    'link' => '',
    'link_remove' => '',
    'owner' => 'nodequeue',
    'show_in_ui' => 1,
    'show_in_tab' => 1,
    'show_in_links' => 0,
    'reference' => 0,
    'reverse' => 0,
    'i18n' => 0,
    'subqueues' => 1,
    'types' => array(
      0 => 'advertisement',
    ),
    'roles' => array(),
    'count' => 0,
  );

  return $nodequeues;
}

/**
 * Implements hook_flag_default_flags().
 */
function adoption_flag_default_flags() {
  $flags = array();
  // Exported flag: "Adopted flag".
  $flags['adopted_flag'] = array(
    'entity_type' => 'node',
    'title' => 'Adopted flag',
    'global' => 1,
    'types' => array(
      0 => 'pet_for_adoption',
    ),
    'flag_short' => 'Pet has been adopted',
    'flag_long' => '',
    'flag_message' => '',
    'unflag_short' => 'Cancel \'pet adopted\'',
    'unflag_long' => '',
    'unflag_message' => '',
    'unflag_denied_text' => '',
    'link_type' => 'confirm',
    'weight' => 0,
    'show_in_links' => array(
      'full' => 'full',
      'teaser' => 'teaser',
      'rss' => 0,
      'search_index' => 0,
      'search_result' => 0,
      'diff_standard' => 0,
      'node_gallery_node_thumbnail' => 0,
      'token' => 0,
    ),
    'show_as_field' => 0,
    'show_on_form' => 0,
    'access_author' => 'own',
    'show_contextual_link' => 0,
    'i18n' => 0,
    'flag_confirmation' => 'Are you sure you want to mark this as \'ADOPTED\'?',
    'unflag_confirmation' => 'Are you sure you want to cancel the \'ADOPTED\' status?',
    'module' => 'adoption',
    'locked' => array(
      0 => 'name',
    ),
    'api_version' => 3,
  );
  return $flags;

}

/**
 * Implements hook_node_info().
 */
function adoption_node_info() {
  $items = array(
    'pet_for_adoption' => array(
      'name' => t('Pet for adoption'),
      'base' => 'node_content',
      'description' => t('Put up a pet for adoption'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  return $items;
}
