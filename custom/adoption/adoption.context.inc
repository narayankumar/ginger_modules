<?php
/**
 * @file
 * adoption.context.inc
 */

/**
 * Implements hook_context_default_contexts().
 */
function adoption_context_default_contexts() {
  $export = array();

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'adoption_blocks';
  $context->description = 'Blocks on adoption nodes and teasers page';
  $context->tag = '';
  $context->conditions = array(
    'node' => array(
      'values' => array(
        'pet_for_adoption' => 'pet_for_adoption',
      ),
      'options' => array(
        'node_form' => '1',
      ),
    ),
    'views' => array(
      'values' => array(
        'adoption_teasers_page' => 'adoption_teasers_page',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'views-ad_block_nodequeues-block_5' => array(
          'module' => 'views',
          'delta' => 'ad_block_nodequeues-block_5',
          'region' => 'sidebar_second',
          'weight' => '-10',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Blocks on adoption nodes and teasers page');
  $export['adoption_blocks'] = $context;

  return $export;
}
