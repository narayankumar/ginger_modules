<?php
/**
 * @file
 * found_pet.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function found_pet_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'create found_pet content'.
  $permissions['create found_pet content'] = array(
    'name' => 'create found_pet content',
    'roles' => array(
      'administrator' => 'administrator',
      'directory user' => 'directory user',
      'editor' => 'editor',
      'forum monitor' => 'forum monitor',
      'registered user' => 'registered user',
      'retailer' => 'retailer',
      'site admin' => 'site admin',
      'writer' => 'writer',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete any found_pet content'.
  $permissions['delete any found_pet content'] = array(
    'name' => 'delete any found_pet content',
    'roles' => array(
      'administrator' => 'administrator',
      'editor' => 'editor',
      'site admin' => 'site admin',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete own found_pet content'.
  $permissions['delete own found_pet content'] = array(
    'name' => 'delete own found_pet content',
    'roles' => array(
      'administrator' => 'administrator',
      'directory user' => 'directory user',
      'editor' => 'editor',
      'forum monitor' => 'forum monitor',
      'registered user' => 'registered user',
      'retailer' => 'retailer',
      'site admin' => 'site admin',
      'writer' => 'writer',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit any found_pet content'.
  $permissions['edit any found_pet content'] = array(
    'name' => 'edit any found_pet content',
    'roles' => array(
      'administrator' => 'administrator',
      'editor' => 'editor',
      'site admin' => 'site admin',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit own found_pet content'.
  $permissions['edit own found_pet content'] = array(
    'name' => 'edit own found_pet content',
    'roles' => array(
      'administrator' => 'administrator',
      'directory user' => 'directory user',
      'editor' => 'editor',
      'forum monitor' => 'forum monitor',
      'registered user' => 'registered user',
      'retailer' => 'retailer',
      'site admin' => 'site admin',
      'writer' => 'writer',
    ),
    'module' => 'node',
  );

  // Exported permission: 'flag pet_re_united'.
  $permissions['flag pet_re_united'] = array(
    'name' => 'flag pet_re_united',
    'roles' => array(
      'administrator' => 'administrator',
      'authenticated user' => 'authenticated user',
      'directory user' => 'directory user',
      'editor' => 'editor',
      'forum monitor' => 'forum monitor',
      'registered user' => 'registered user',
      'retailer' => 'retailer',
      'site admin' => 'site admin',
      'writer' => 'writer',
    ),
    'module' => 'flag',
  );

  // Exported permission: 'publish button publish any found_pet'.
  $permissions['publish button publish any found_pet'] = array(
    'name' => 'publish button publish any found_pet',
    'roles' => array(
      'administrator' => 'administrator',
      'editor' => 'editor',
      'site admin' => 'site admin',
    ),
    'module' => 'publish_button',
  );

  // Exported permission: 'publish button publish editable found_pet'.
  $permissions['publish button publish editable found_pet'] = array(
    'name' => 'publish button publish editable found_pet',
    'roles' => array(
      'administrator' => 'administrator',
      'editor' => 'editor',
      'site admin' => 'site admin',
    ),
    'module' => 'publish_button',
  );

  // Exported permission: 'publish button publish own found_pet'.
  $permissions['publish button publish own found_pet'] = array(
    'name' => 'publish button publish own found_pet',
    'roles' => array(
      'administrator' => 'administrator',
      'editor' => 'editor',
      'site admin' => 'site admin',
    ),
    'module' => 'publish_button',
  );

  // Exported permission: 'publish button unpublish any found_pet'.
  $permissions['publish button unpublish any found_pet'] = array(
    'name' => 'publish button unpublish any found_pet',
    'roles' => array(
      'administrator' => 'administrator',
      'editor' => 'editor',
      'site admin' => 'site admin',
    ),
    'module' => 'publish_button',
  );

  // Exported permission: 'publish button unpublish editable found_pet'.
  $permissions['publish button unpublish editable found_pet'] = array(
    'name' => 'publish button unpublish editable found_pet',
    'roles' => array(
      'administrator' => 'administrator',
      'editor' => 'editor',
      'site admin' => 'site admin',
    ),
    'module' => 'publish_button',
  );

  // Exported permission: 'publish button unpublish own found_pet'.
  $permissions['publish button unpublish own found_pet'] = array(
    'name' => 'publish button unpublish own found_pet',
    'roles' => array(
      'administrator' => 'administrator',
      'editor' => 'editor',
      'site admin' => 'site admin',
    ),
    'module' => 'publish_button',
  );

  // Exported permission: 'unflag pet_re_united'.
  $permissions['unflag pet_re_united'] = array(
    'name' => 'unflag pet_re_united',
    'roles' => array(
      'administrator' => 'administrator',
      'editor' => 'editor',
      'site admin' => 'site admin',
    ),
    'module' => 'flag',
  );

  return $permissions;
}
