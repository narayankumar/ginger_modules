<?php
/**
 * @file
 * found_pet.features.field_base.inc
 */

/**
 * Implements hook_field_default_field_bases().
 */
function found_pet_field_default_field_bases() {
  $field_bases = array();

  // Exported field_base: 'field_found_pet_reunited'
  $field_bases['field_found_pet_reunited'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_found_pet_reunited',
    'foreign keys' => array(),
    'indexes' => array(),
    'locked' => 0,
    'module' => 'field_hidden',
    'settings' => array(
      'max_length' => 60,
    ),
    'translatable' => 0,
    'type' => 'field_hidden_text',
  );

  // Exported field_base: 'field_share_found_pet_info'
  $field_bases['field_share_found_pet_info'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_share_found_pet_info',
    'foreign keys' => array(),
    'indexes' => array(),
    'locked' => 0,
    'module' => 'addthis',
    'settings' => array(
      'max_length' => 255,
    ),
    'translatable' => 0,
    'type' => 'addthis',
  );

  return $field_bases;
}
