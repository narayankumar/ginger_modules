<?php
/**
 * @file
 * photo_contest.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function photo_contest_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "ds" && $api == "ds") {
    return array("version" => "1");
  }
  if ($module == "page_manager" && $api == "pages_default") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function photo_contest_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_flag_default_flags().
 */
function photo_contest_flag_default_flags() {
  $flags = array();
  // Exported flag: "Enter photo in Contest".
  $flags['enter_photo_in_contest'] = array(
    'entity_type' => 'node',
    'title' => 'Enter photo in Contest',
    'global' => 0,
    'types' => array(
      0 => 'node_gallery_item',
    ),
    'flag_short' => 'Submit this photo to a contest',
    'flag_long' => '',
    'flag_message' => '',
    'unflag_short' => 'Already submitted - reset now',
    'unflag_long' => '',
    'unflag_message' => '',
    'unflag_denied_text' => '',
    'link_type' => 'confirm',
    'weight' => 0,
    'show_in_links' => array(
      'full' => 'full',
      'teaser' => 'teaser',
      'rss' => 0,
      'search_index' => 0,
      'search_result' => 0,
      'diff_standard' => 0,
      'node_gallery_node_thumbnail' => 0,
      'token' => 0,
      'revision' => 0,
    ),
    'show_as_field' => 0,
    'show_on_form' => 0,
    'access_author' => 'own',
    'show_contextual_link' => 0,
    'i18n' => 0,
    'flag_confirmation' => 'You can only enter if there is an open Contest. Check below.',
    'unflag_confirmation' => 'This does NOT cancel your contest entry, only resets it so you can enter again for other contests. Go ahead?',
    'module' => 'photo_contest',
    'locked' => array(
      0 => 'name',
    ),
    'api_version' => 3,
  );
  return $flags;

}

/**
 * Implements hook_image_default_styles().
 */
function photo_contest_image_default_styles() {
  $styles = array();

  // Exported image style: 160x.
  $styles['160x'] = array(
    'name' => '160x',
    'label' => '160x',
    'effects' => array(
      1 => array(
        'label' => 'Scale',
        'help' => 'Scaling will maintain the aspect-ratio of the original image. If only a single dimension is specified, the other dimension will be calculated.',
        'effect callback' => 'image_scale_effect',
        'dimensions callback' => 'image_scale_dimensions',
        'form callback' => 'image_scale_form',
        'summary theme' => 'image_scale_summary',
        'module' => 'image',
        'name' => 'image_scale',
        'data' => array(
          'width' => 160,
          'height' => '',
          'upscale' => 0,
        ),
        'weight' => 1,
      ),
    ),
  );

  return $styles;
}

/**
 * Implements hook_node_info().
 */
function photo_contest_node_info() {
  $items = array(
    'photo_contest' => array(
      'name' => t('Photo Contest'),
      'base' => 'node_content',
      'description' => t('A contest for registered users to participate with their original photos'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'photo_contest_entry' => array(
      'name' => t('Submit your photo'),
      'base' => 'node_content',
      'description' => t('A single photo as entry to a Photo Contest by a registered user'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => t('IF YOU WANT TO ENTER ONE OF YOUR ALREADY UPLOADED PHOTOS, GO TO <a href="../../user">\'My Stuff\' Page</a> AND CLICK ON \'PHOTOS\' TAB. By submitting an entry you agree to the Terms and Conditions as specified in the <a href="">Terms of Use</a> of this site'),
    ),
  );
  return $items;
}
