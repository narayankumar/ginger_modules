<?php
/**
 * @file
 * photo_contest.ds.inc
 */

/**
 * Implements hook_ds_field_settings_info().
 */
function photo_contest_ds_field_settings_info() {
  $export = array();

  $ds_fieldsetting = new stdClass();
  $ds_fieldsetting->api_version = 1;
  $ds_fieldsetting->id = 'node|photo_contest|default';
  $ds_fieldsetting->entity_type = 'node';
  $ds_fieldsetting->bundle = 'photo_contest';
  $ds_fieldsetting->view_mode = 'default';
  $ds_fieldsetting->settings = array(
    'create_a_contest_entry' => array(
      'weight' => '5',
      'label' => 'hidden',
      'format' => 'default',
      'formatter_settings' => array(
        'show_title' => 0,
        'title_wrapper' => '',
        'ctools' => 'a:3:{s:4:"conf";a:5:{s:5:"types";a:1:{s:19:"photo_contest_entry";s:19:"photo_contest_entry";}s:10:"field_name";s:23:"field_contest_reference";s:7:"context";s:25:"argument_entity_id:node_1";s:14:"override_title";i:1;s:19:"override_title_text";s:17:"Submit your photo";}s:4:"type";s:16:"node_prepopulate";s:7:"subtype";s:16:"node_prepopulate";}',
        'load_terms' => 0,
      ),
    ),
    'title' => array(
      'weight' => '0',
      'label' => 'hidden',
      'format' => 'default',
    ),
  );
  $export['node|photo_contest|default'] = $ds_fieldsetting;

  return $export;
}

/**
 * Implements hook_ds_custom_fields_info().
 */
function photo_contest_ds_custom_fields_info() {
  $export = array();

  $ds_field = new stdClass();
  $ds_field->api_version = 1;
  $ds_field->field = 'create_a_contest_entry';
  $ds_field->label = 'Submit your photo';
  $ds_field->field_type = 7;
  $ds_field->entities = array(
    'node' => 'node',
  );
  $ds_field->ui_limit = '';
  $ds_field->properties = array(
    'default' => array(),
    'settings' => array(
      'show_title' => array(
        'type' => 'checkbox',
      ),
      'title_wrapper' => array(
        'type' => 'textfield',
        'description' => 'Eg: h1, h2, p',
      ),
      'ctools' => array(
        'type' => 'ctools',
      ),
    ),
  );
  $export['create_a_contest_entry'] = $ds_field;

  return $export;
}

/**
 * Implements hook_ds_layout_settings_info().
 */
function photo_contest_ds_layout_settings_info() {
  $export = array();

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|photo_contest|default';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'photo_contest';
  $ds_layout->view_mode = 'default';
  $ds_layout->layout = 'ds_1col';
  $ds_layout->settings = array(
    'regions' => array(
      'ds_content' => array(
        0 => 'title',
        1 => 'field_contest_image',
        2 => 'field_contest_description',
        3 => 'field_contest_closed',
        4 => 'field_contest_last_date',
        5 => 'create_a_contest_entry',
        6 => 'photo_contest_entries_listed_under_contest_entity_view_1',
      ),
    ),
    'fields' => array(
      'title' => 'ds_content',
      'field_contest_image' => 'ds_content',
      'field_contest_description' => 'ds_content',
      'field_contest_closed' => 'ds_content',
      'field_contest_last_date' => 'ds_content',
      'create_a_contest_entry' => 'ds_content',
      'photo_contest_entries_listed_under_contest_entity_view_1' => 'ds_content',
    ),
    'classes' => array(),
    'wrappers' => array(
      'ds_content' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
  );
  $export['node|photo_contest|default'] = $ds_layout;

  return $export;
}
