<?php
/**
 * @file
 * photo_contest.rules_defaults.inc
 */

/**
 * Implements hook_default_rules_configuration().
 */
function photo_contest_default_rules_configuration() {
  $items = array();
  $items['rules_actions_on_publishing_contest_entry'] = entity_import('rules_config', '{ "rules_actions_on_publishing_contest_entry" : {
      "LABEL" : "Actions on publishing Contest Entry",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "REQUIRES" : [ "rules" ],
      "ON" : {
        "node_update--photo_contest_entry" : { "bundle" : "photo_contest_entry" },
        "node_update--video_contest_entry" : { "bundle" : "video_contest_entry" },
        "node_update--opinion" : { "bundle" : "opinion" }
      },
      "IF" : [
        { "data_is" : { "data" : [ "node-unchanged:status" ], "value" : "0" } },
        { "data_is" : { "data" : [ "node:status" ], "value" : "1" } }
      ],
      "DO" : [
        { "mail" : {
            "to" : "[node:author:mail]",
            "subject" : "Your submission has been published",
            "message" : "hi [node:author]:\\r\\n\\r\\nYour submission \\u0027[node:title]\\u0027 has been accepted for publication.\\r\\n\\r\\nYou can visit it at: [node:url]\\r\\n\\r\\nCheers. Keep it coming!\\r\\n\\r\\n-- Editor at Gingertail.in",
            "from" : "Editor@gingertail.in",
            "language" : [ "" ]
          }
        },
        { "component_rules_create_photo_node_if_necessary" : { "node" : [ "node" ] } },
        { "component_rules_change_author_video_contest" : { "node" : [ "node" ] } },
        { "redirect" : { "url" : "\\u003Cfront\\u003E" } },
        { "drupal_message" : { "message" : "Submission(s) have been published and confirmation mail has been sent to the author(s)." } }
      ]
    }
  }');
  $items['rules_change_author_video_contest'] = entity_import('rules_config', '{ "rules_change_author_video_contest" : {
      "LABEL" : "Change author of video contest entry",
      "PLUGIN" : "rule",
      "OWNER" : "rules",
      "REQUIRES" : [ "rules" ],
      "USES VARIABLES" : { "node" : { "label" : "Node", "type" : "node" } },
      "IF" : [
        { "OR" : [
            { "node_is_of_type" : {
                "node" : [ "node" ],
                "type" : { "value" : { "video_contest_entry" : "video_contest_entry" } }
              }
            },
            { "node_is_of_type" : { "node" : [ "node" ], "type" : { "value" : { "opinion" : "opinion" } } } }
          ]
        }
      ],
      "DO" : [
        { "data_set" : {
            "data" : [ "node:field-contest-submitter" ],
            "value" : [ "node:author" ]
          }
        },
        { "data_set" : { "data" : [ "node:author" ], "value" : [ "site:current-user" ] } }
      ]
    }
  }');
  $items['rules_create_photo_node_if_necessary'] = entity_import('rules_config', '{ "rules_create_photo_node_if_necessary" : {
      "LABEL" : "Create photo node if necessary",
      "PLUGIN" : "rule set",
      "OWNER" : "rules",
      "REQUIRES" : [ "rules" ],
      "USES VARIABLES" : { "node" : { "label" : "Node", "type" : "node" } },
      "RULES" : [
        { "RULE" : {
            "PROVIDE" : { "entity_created" : { "photo_node" : "Photo node" } },
            "IF" : [
              { "node_is_of_type" : {
                  "node" : [ "node" ],
                  "type" : { "value" : { "photo_contest_entry" : "photo_contest_entry" } }
                }
              },
              { "data_is" : { "data" : [ "node:field-add-contest-photo-album" ], "value" : 1 } }
            ],
            "DO" : [
              { "entity_create" : {
                  "USING" : {
                    "type" : "node",
                    "param_type" : "node_gallery_item",
                    "param_title" : [ "node:title" ],
                    "param_author" : [ "node:author" ]
                  },
                  "PROVIDE" : { "entity_created" : { "photo_node" : "Photo node" } }
                }
              },
              { "data_set" : { "data" : [ "photo-node:body:value" ], "value" : [ "node:body:value" ] } },
              { "data_set" : {
                  "data" : [ "photo-node:node-gallery-media:file" ],
                  "value" : [ "node:field-contest-image:file" ]
                }
              },
              { "entity_save" : { "data" : [ "photo-node" ], "immediate" : 1 } }
            ],
            "LABEL" : "Create photo node"
          }
        },
        { "RULE" : {
            "IF" : [
              { "node_is_of_type" : {
                  "node" : [ "node" ],
                  "type" : { "value" : { "photo_contest_entry" : "photo_contest_entry" } }
                }
              }
            ],
            "DO" : [
              { "data_set" : {
                  "data" : [ "node:field-contest-submitter" ],
                  "value" : [ "node:author" ]
                }
              },
              { "data_set" : { "data" : [ "node:author" ], "value" : [ "site:current-user" ] } }
            ],
            "LABEL" : "Change authorship after saving original author"
          }
        }
      ]
    }
  }');
  $items['rules_enter_user_photo_in_contest'] = entity_import('rules_config', '{ "rules_enter_user_photo_in_contest" : {
      "LABEL" : "Enter user photo in contest",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "REQUIRES" : [ "rules", "flag" ],
      "ON" : { "flag_flagged_enter_photo_in_contest" : [] },
      "IF" : [
        { "entity_has_field" : { "entity" : [ "flagged-node" ], "field" : "node_gallery_media" } },
        { "flag_flagged_node" : {
            "flag" : "enter_photo_in_contest",
            "node" : [ "flagged-node" ],
            "flagging_user" : [ "flagging_user" ]
          }
        }
      ],
      "DO" : [
        { "entity_query" : {
            "USING" : {
              "type" : "node",
              "property" : "field_contest_closed",
              "value" : 0,
              "limit" : "1"
            },
            "PROVIDE" : { "entity_fetched" : { "active_contest" : "Active Contest" } }
          }
        },
        { "entity_create" : {
            "USING" : {
              "type" : "node",
              "param_type" : "photo_contest_entry",
              "param_title" : [ "flagged-node:title" ],
              "param_author" : [ "flagged-node:author" ]
            },
            "PROVIDE" : { "entity_created" : { "photo_node" : "Photo node" } }
          }
        },
        { "data_set" : {
            "data" : [ "photo-node:field-contest-reference" ],
            "value" : [ "active-contest:0" ]
          }
        },
        { "data_set" : {
            "data" : [ "photo-node:body:value" ],
            "value" : [ "flagged-node:body:value" ]
          }
        },
        { "data_set" : {
            "data" : [ "photo-node:field-contest-image:file" ],
            "value" : [ "flagged-node:node-gallery-media:file" ]
          }
        },
        { "entity_save" : { "data" : [ "photo-node" ], "immediate" : 1 } }
      ]
    }
  }');
  return $items;
}
