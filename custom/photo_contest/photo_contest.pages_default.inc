<?php
/**
 * @file
 * photo_contest.pages_default.inc
 */

/**
 * Implements hook_default_page_manager_pages().
 */
function photo_contest_default_page_manager_pages() {
  $page = new stdClass();
  $page->disabled = FALSE; /* Edit this to true to make a default page disabled initially */
  $page->api_version = 1;
  $page->name = 'contests_page';
  $page->task = 'page';
  $page->admin_title = 'Contests page';
  $page->admin_description = 'display page for current photo and video contests, one each, divided vertically';
  $page->path = 'contests';
  $page->access = array();
  $page->menu = array(
    'type' => 'normal',
    'title' => 'Contests',
    'name' => 'main-menu',
    'weight' => '10',
    'parent' => array(
      'type' => 'none',
      'title' => '',
      'name' => 'navigation',
      'weight' => '0',
    ),
  );
  $page->arguments = array();
  $page->conf = array(
    'admin_paths' => FALSE,
  );
  $page->default_handlers = array();
  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'page_contests_page_panel_context';
  $handler->task = 'page';
  $handler->subtask = 'contests_page';
  $handler->handler = 'panel_context';
  $handler->weight = 0;
  $handler->conf = array(
    'title' => 'Contests Panel',
    'no_blocks' => 0,
    'pipeline' => 'standard',
    'body_classes_to_remove' => '',
    'body_classes_to_add' => '',
    'css_id' => '',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
  );
  $display = new panels_display();
  $display->layout = 'twocol_stacked';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'left' => NULL,
      'right' => NULL,
      'top' => NULL,
      'bottom' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = '';
  $display->uuid = '1768b1cd-40f8-42f9-bf09-6c1315ce83e2';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-62577580-2b0e-4654-8c66-826a028567ca';
    $pane->panel = 'bottom';
    $pane->type = 'views';
    $pane->subtype = 'contest_management_for_admin';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_pager_settings' => 0,
      'use_pager' => 0,
      'nodes_per_page' => '',
      'pager_id' => '0',
      'offset' => '0',
      'more_link' => 0,
      'feed_icons' => 0,
      'panel_args' => 0,
      'link_to_view' => 0,
      'args' => '',
      'url' => '',
      'display' => 'page',
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '62577580-2b0e-4654-8c66-826a028567ca';
    $display->content['new-62577580-2b0e-4654-8c66-826a028567ca'] = $pane;
    $display->panels['bottom'][0] = 'new-62577580-2b0e-4654-8c66-826a028567ca';
    $pane = new stdClass();
    $pane->pid = 'new-2ca7af02-79ec-42cb-bfe7-c7ac74a27fcb';
    $pane->panel = 'left';
    $pane->type = 'views';
    $pane->subtype = 'photo_contest_teasers_page';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_pager_settings' => 0,
      'use_pager' => 0,
      'nodes_per_page' => '1',
      'pager_id' => '0',
      'offset' => '0',
      'more_link' => 0,
      'feed_icons' => 0,
      'panel_args' => 0,
      'link_to_view' => 0,
      'args' => '',
      'url' => '',
      'display' => 'page',
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '2ca7af02-79ec-42cb-bfe7-c7ac74a27fcb';
    $display->content['new-2ca7af02-79ec-42cb-bfe7-c7ac74a27fcb'] = $pane;
    $display->panels['left'][0] = 'new-2ca7af02-79ec-42cb-bfe7-c7ac74a27fcb';
    $pane = new stdClass();
    $pane->pid = 'new-e1eca2c6-d3d0-4f42-87ec-776741df863c';
    $pane->panel = 'left';
    $pane->type = 'views';
    $pane->subtype = 'photo_contest_teasers_page';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_pager_settings' => 0,
      'use_pager' => 0,
      'nodes_per_page' => '5',
      'pager_id' => '0',
      'offset' => '0',
      'more_link' => 0,
      'feed_icons' => 0,
      'panel_args' => 0,
      'link_to_view' => 0,
      'args' => '',
      'url' => '',
      'display' => 'block_2',
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 1;
    $pane->locks = array();
    $pane->uuid = 'e1eca2c6-d3d0-4f42-87ec-776741df863c';
    $display->content['new-e1eca2c6-d3d0-4f42-87ec-776741df863c'] = $pane;
    $display->panels['left'][1] = 'new-e1eca2c6-d3d0-4f42-87ec-776741df863c';
    $pane = new stdClass();
    $pane->pid = 'new-262d7c35-7bab-40d3-8871-acd35b52af17';
    $pane->panel = 'right';
    $pane->type = 'views';
    $pane->subtype = 'photo_contest_teasers_page';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_pager_settings' => 0,
      'use_pager' => 0,
      'nodes_per_page' => '1',
      'pager_id' => '0',
      'offset' => '0',
      'more_link' => 0,
      'feed_icons' => 0,
      'panel_args' => 0,
      'link_to_view' => 0,
      'args' => '',
      'url' => '',
      'display' => 'attachment_1',
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '262d7c35-7bab-40d3-8871-acd35b52af17';
    $display->content['new-262d7c35-7bab-40d3-8871-acd35b52af17'] = $pane;
    $display->panels['right'][0] = 'new-262d7c35-7bab-40d3-8871-acd35b52af17';
    $pane = new stdClass();
    $pane->pid = 'new-5c48dd34-f52d-403d-ba18-de32e725f7ae';
    $pane->panel = 'right';
    $pane->type = 'views';
    $pane->subtype = 'photo_contest_teasers_page';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_pager_settings' => 0,
      'use_pager' => 0,
      'nodes_per_page' => '5',
      'pager_id' => '0',
      'offset' => '0',
      'more_link' => 0,
      'feed_icons' => 0,
      'panel_args' => 0,
      'link_to_view' => 0,
      'args' => '',
      'url' => '',
      'display' => 'block_3',
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 1;
    $pane->locks = array();
    $pane->uuid = '5c48dd34-f52d-403d-ba18-de32e725f7ae';
    $display->content['new-5c48dd34-f52d-403d-ba18-de32e725f7ae'] = $pane;
    $display->panels['right'][1] = 'new-5c48dd34-f52d-403d-ba18-de32e725f7ae';
  $display->hide_title = PANELS_TITLE_NONE;
  $display->title_pane = '0';
  $handler->conf['display'] = $display;
  $page->default_handlers[$handler->name] = $handler;
  $pages['contests_page'] = $page;

  $page = new stdClass();
  $page->disabled = FALSE; /* Edit this to true to make a default page disabled initially */
  $page->api_version = 1;
  $page->name = 'photo_contest_node_add';
  $page->task = 'page';
  $page->admin_title = 'Photo contest node add';
  $page->admin_description = '';
  $page->path = 'contests/add';
  $page->access = array(
    'plugins' => array(
      0 => array(
        'name' => 'perm',
        'settings' => array(
          'perm' => 'create photo_contest content',
        ),
        'context' => 'logged-in-user',
        'not' => FALSE,
      ),
    ),
    'logic' => 'and',
    'type' => 'none',
    'settings' => NULL,
  );
  $page->menu = array(
    'type' => 'action',
    'title' => 'Create Photo Contest',
    'name' => 'navigation',
    'weight' => '0',
    'parent' => array(
      'type' => 'none',
      'title' => '',
      'name' => 'navigation',
      'weight' => '0',
    ),
  );
  $page->arguments = array();
  $page->conf = array(
    'admin_paths' => TRUE,
  );
  $page->default_handlers = array();
  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'page_photo_contest_node_add_menu_context';
  $handler->task = 'page';
  $handler->subtask = 'photo_contest_node_add';
  $handler->handler = 'menu_context';
  $handler->weight = 0;
  $handler->conf = array(
    'title' => 'Context Admin',
    'no_blocks' => FALSE,
    'css_id' => '',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
    'context_admin_options' => 'node_create_menu',
    'context_admin_options_items' => 'photo_contest',
    'submitted_context' => 'empty',
    'context_admin_use_node_edit' => 1,
  );
  $page->default_handlers[$handler->name] = $handler;
  $pages['photo_contest_node_add'] = $page;

  return $pages;

}
