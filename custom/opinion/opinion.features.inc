<?php
/**
 * @file
 * opinion.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function opinion_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "page_manager" && $api == "pages_default") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function opinion_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_node_info().
 */
function opinion_node_info() {
  $items = array(
    'opinion' => array(
      'name' => t('Your Opinion'),
      'base' => 'node_content',
      'description' => t('An essay from registered user on a subject by invitation'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'opinion_invite' => array(
      'name' => t('Opinion Invite'),
      'base' => 'node_content',
      'description' => t('use this to invite user opinion on article items or news items, etc.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
