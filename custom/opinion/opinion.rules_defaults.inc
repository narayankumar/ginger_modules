<?php
/**
 * @file
 * opinion.rules_defaults.inc
 */

/**
 * Implements hook_default_rules_configuration().
 */
function opinion_default_rules_configuration() {
  $items = array();
  $items['rules_auto_populate_term_reference_fields_for_your_opinion'] = entity_import('rules_config', '{ "rules_auto_populate_term_reference_fields_for_your_opinion" : {
      "LABEL" : "Auto populate term reference fields for your opinion",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "REQUIRES" : [ "rules" ],
      "ON" : { "node_insert--opinion" : { "bundle" : "opinion" } },
      "IF" : [
        { "entity_has_field" : { "entity" : [ "node" ], "field" : "field_opinion_invite_ref" } },
        { "entity_has_field" : {
            "entity" : [ "node:field-opinion-invite-ref" ],
            "field" : "field_magazine_category"
          }
        },
        { "entity_has_field" : {
            "entity" : [ "node:field-opinion-invite-ref" ],
            "field" : "field_voices_category"
          }
        }
      ],
      "DO" : [
        { "data_set" : {
            "data" : [ "node:field-magazine-category" ],
            "value" : [ "node:field-opinion-invite-ref:field-magazine-category" ]
          }
        },
        { "data_set" : {
            "data" : [ "node:field-voices-category" ],
            "value" : [ "node:field-opinion-invite-ref:field-voices-category" ]
          }
        },
        { "entity_save" : { "data" : [ "node" ], "immediate" : 1 } }
      ]
    }
  }');
  return $items;
}
