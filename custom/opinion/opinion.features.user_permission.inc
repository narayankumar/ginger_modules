<?php
/**
 * @file
 * opinion.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function opinion_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'create opinion content'.
  $permissions['create opinion content'] = array(
    'name' => 'create opinion content',
    'roles' => array(
      'administrator' => 'administrator',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'node',
  );

  // Exported permission: 'create opinion_invite content'.
  $permissions['create opinion_invite content'] = array(
    'name' => 'create opinion_invite content',
    'roles' => array(
      'administrator' => 'administrator',
      'editor' => 'editor',
      'site admin' => 'site admin',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete any opinion content'.
  $permissions['delete any opinion content'] = array(
    'name' => 'delete any opinion content',
    'roles' => array(
      'administrator' => 'administrator',
      'editor' => 'editor',
      'site admin' => 'site admin',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete any opinion_invite content'.
  $permissions['delete any opinion_invite content'] = array(
    'name' => 'delete any opinion_invite content',
    'roles' => array(
      'administrator' => 'administrator',
      'editor' => 'editor',
      'site admin' => 'site admin',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete own opinion content'.
  $permissions['delete own opinion content'] = array(
    'name' => 'delete own opinion content',
    'roles' => array(
      'administrator' => 'administrator',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete own opinion_invite content'.
  $permissions['delete own opinion_invite content'] = array(
    'name' => 'delete own opinion_invite content',
    'roles' => array(
      'administrator' => 'administrator',
      'editor' => 'editor',
      'site admin' => 'site admin',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit any opinion content'.
  $permissions['edit any opinion content'] = array(
    'name' => 'edit any opinion content',
    'roles' => array(
      'administrator' => 'administrator',
      'editor' => 'editor',
      'site admin' => 'site admin',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit any opinion_invite content'.
  $permissions['edit any opinion_invite content'] = array(
    'name' => 'edit any opinion_invite content',
    'roles' => array(
      'administrator' => 'administrator',
      'editor' => 'editor',
      'site admin' => 'site admin',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit own opinion content'.
  $permissions['edit own opinion content'] = array(
    'name' => 'edit own opinion content',
    'roles' => array(
      'administrator' => 'administrator',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit own opinion_invite content'.
  $permissions['edit own opinion_invite content'] = array(
    'name' => 'edit own opinion_invite content',
    'roles' => array(
      'administrator' => 'administrator',
      'editor' => 'editor',
      'site admin' => 'site admin',
    ),
    'module' => 'node',
  );

  // Exported permission: 'publish button publish any opinion'.
  $permissions['publish button publish any opinion'] = array(
    'name' => 'publish button publish any opinion',
    'roles' => array(
      'administrator' => 'administrator',
      'editor' => 'editor',
      'site admin' => 'site admin',
    ),
    'module' => 'publish_button',
  );

  // Exported permission: 'publish button publish any opinion_invite'.
  $permissions['publish button publish any opinion_invite'] = array(
    'name' => 'publish button publish any opinion_invite',
    'roles' => array(
      'administrator' => 'administrator',
      'editor' => 'editor',
      'site admin' => 'site admin',
    ),
    'module' => 'publish_button',
  );

  // Exported permission: 'publish button publish editable opinion'.
  $permissions['publish button publish editable opinion'] = array(
    'name' => 'publish button publish editable opinion',
    'roles' => array(
      'administrator' => 'administrator',
      'editor' => 'editor',
      'site admin' => 'site admin',
    ),
    'module' => 'publish_button',
  );

  // Exported permission: 'publish button publish editable opinion_invite'.
  $permissions['publish button publish editable opinion_invite'] = array(
    'name' => 'publish button publish editable opinion_invite',
    'roles' => array(
      'administrator' => 'administrator',
      'editor' => 'editor',
      'site admin' => 'site admin',
    ),
    'module' => 'publish_button',
  );

  // Exported permission: 'publish button publish own opinion'.
  $permissions['publish button publish own opinion'] = array(
    'name' => 'publish button publish own opinion',
    'roles' => array(
      'administrator' => 'administrator',
      'editor' => 'editor',
      'site admin' => 'site admin',
    ),
    'module' => 'publish_button',
  );

  // Exported permission: 'publish button publish own opinion_invite'.
  $permissions['publish button publish own opinion_invite'] = array(
    'name' => 'publish button publish own opinion_invite',
    'roles' => array(
      'administrator' => 'administrator',
      'editor' => 'editor',
      'site admin' => 'site admin',
    ),
    'module' => 'publish_button',
  );

  // Exported permission: 'publish button unpublish any opinion'.
  $permissions['publish button unpublish any opinion'] = array(
    'name' => 'publish button unpublish any opinion',
    'roles' => array(
      'administrator' => 'administrator',
      'editor' => 'editor',
      'site admin' => 'site admin',
    ),
    'module' => 'publish_button',
  );

  // Exported permission: 'publish button unpublish any opinion_invite'.
  $permissions['publish button unpublish any opinion_invite'] = array(
    'name' => 'publish button unpublish any opinion_invite',
    'roles' => array(
      'administrator' => 'administrator',
      'editor' => 'editor',
      'site admin' => 'site admin',
    ),
    'module' => 'publish_button',
  );

  // Exported permission: 'publish button unpublish editable opinion'.
  $permissions['publish button unpublish editable opinion'] = array(
    'name' => 'publish button unpublish editable opinion',
    'roles' => array(
      'administrator' => 'administrator',
      'editor' => 'editor',
      'site admin' => 'site admin',
    ),
    'module' => 'publish_button',
  );

  // Exported permission: 'publish button unpublish editable opinion_invite'.
  $permissions['publish button unpublish editable opinion_invite'] = array(
    'name' => 'publish button unpublish editable opinion_invite',
    'roles' => array(
      'administrator' => 'administrator',
      'editor' => 'editor',
      'site admin' => 'site admin',
    ),
    'module' => 'publish_button',
  );

  // Exported permission: 'publish button unpublish own opinion'.
  $permissions['publish button unpublish own opinion'] = array(
    'name' => 'publish button unpublish own opinion',
    'roles' => array(
      'administrator' => 'administrator',
      'editor' => 'editor',
      'site admin' => 'site admin',
    ),
    'module' => 'publish_button',
  );

  // Exported permission: 'publish button unpublish own opinion_invite'.
  $permissions['publish button unpublish own opinion_invite'] = array(
    'name' => 'publish button unpublish own opinion_invite',
    'roles' => array(
      'administrator' => 'administrator',
      'editor' => 'editor',
      'site admin' => 'site admin',
    ),
    'module' => 'publish_button',
  );

  return $permissions;
}
