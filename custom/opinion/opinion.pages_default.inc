<?php
/**
 * @file
 * opinion.pages_default.inc
 */

/**
 * Implements hook_default_page_manager_pages().
 */
function opinion_default_page_manager_pages() {
  $page = new stdClass();
  $page->disabled = FALSE; /* Edit this to true to make a default page disabled initially */
  $page->api_version = 1;
  $page->name = 'opinion_invite_manage';
  $page->task = 'page';
  $page->admin_title = 'Opinion-invite manage';
  $page->admin_description = '';
  $page->path = 'opinion-invites/manage';
  $page->access = array(
    'plugins' => array(
      0 => array(
        'name' => 'perm',
        'settings' => array(
          'perm' => 'edit any announcement content',
        ),
        'context' => 'logged-in-user',
        'not' => FALSE,
      ),
    ),
    'logic' => 'and',
    'type' => 'none',
    'settings' => NULL,
  );
  $page->menu = array(
    'type' => 'tab',
    'title' => 'Manage',
    'name' => 'navigation',
    'weight' => '5',
    'parent' => array(
      'type' => 'none',
      'title' => '',
      'name' => 'navigation',
      'weight' => '0',
    ),
  );
  $page->arguments = array();
  $page->conf = array(
    'admin_paths' => TRUE,
  );
  $page->default_handlers = array();
  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'page_opinion_invite_manage_menu_context';
  $handler->task = 'page';
  $handler->subtask = 'opinion_invite_manage';
  $handler->handler = 'menu_context';
  $handler->weight = 0;
  $handler->conf = array(
    'title' => 'Context Admin',
    'no_blocks' => FALSE,
    'css_id' => '',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
    'context_admin_options' => 'views_bulk_menu',
    'context_admin_vbo_machine_name' => 'opinion_invites_manage',
    'context_admin_vbo_views_bulk_node_type' => array(
      'opinion_invite' => 'opinion_invite',
      'advertisement' => 0,
      'announcement' => 0,
      'article' => 0,
      'page' => 0,
      'directory_entry' => 0,
      'guest_column' => 0,
      'forum' => 0,
      'found_pet' => 0,
      'lost_pet' => 0,
      'news' => 0,
      'pet_for_adoption' => 0,
      'pet_video' => 0,
      'node_gallery_item' => 0,
      'node_gallery_gallery' => 0,
      'photo_contest' => 0,
      'poll' => 0,
      'video_contest_entry' => 0,
      'photo_contest_entry' => 0,
      'video_contest' => 0,
    ),
    'context_admin_vbo_views_bulk_published' => 'both',
  );
  $page->default_handlers[$handler->name] = $handler;
  $pages['opinion_invite_manage'] = $page;

  $page = new stdClass();
  $page->disabled = FALSE; /* Edit this to true to make a default page disabled initially */
  $page->api_version = 1;
  $page->name = 'opinion_invite_node_add';
  $page->task = 'page';
  $page->admin_title = 'Opinion-invite node add';
  $page->admin_description = '';
  $page->path = 'opinion-invites/add';
  $page->access = array(
    'plugins' => array(
      0 => array(
        'name' => 'perm',
        'settings' => array(
          'perm' => 'create announcement content',
        ),
        'context' => 'logged-in-user',
        'not' => FALSE,
      ),
    ),
    'logic' => 'and',
    'type' => 'none',
    'settings' => NULL,
  );
  $page->menu = array(
    'type' => 'action',
    'title' => 'Create Opinion Invite',
    'name' => 'navigation',
    'weight' => '0',
    'parent' => array(
      'type' => 'none',
      'title' => '',
      'name' => 'navigation',
      'weight' => '0',
    ),
  );
  $page->arguments = array();
  $page->conf = array(
    'admin_paths' => TRUE,
  );
  $page->default_handlers = array();
  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'page_opinion_invite_node_add_menu_context';
  $handler->task = 'page';
  $handler->subtask = 'opinion_invite_node_add';
  $handler->handler = 'menu_context';
  $handler->weight = 0;
  $handler->conf = array(
    'title' => 'Context Admin',
    'no_blocks' => FALSE,
    'css_id' => '',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
    'context_admin_options' => 'node_create_menu',
    'context_admin_options_items' => 'opinion_invite',
    'submitted_context' => 'empty',
    'context_admin_use_node_edit' => 1,
  );
  $page->default_handlers[$handler->name] = $handler;
  $pages['opinion_invite_node_add'] = $page;

  $page = new stdClass();
  $page->disabled = FALSE; /* Edit this to true to make a default page disabled initially */
  $page->api_version = 1;
  $page->name = 'opinion_invite_node_view';
  $page->task = 'page';
  $page->admin_title = 'Opinion-invite node view';
  $page->admin_description = '';
  $page->path = 'opinion-invites/view';
  $page->access = array(
    'type' => 'none',
    'settings' => NULL,
  );
  $page->menu = array(
    'type' => 'default tab',
    'title' => 'View',
    'name' => 'navigation',
    'weight' => '0',
    'parent' => array(
      'type' => 'none',
      'title' => '',
      'name' => 'navigation',
      'weight' => '0',
    ),
  );
  $page->arguments = array();
  $page->conf = array(
    'admin_paths' => FALSE,
  );
  $page->default_handlers = array();
  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'page_opinion_invite_node_view_http_response';
  $handler->task = 'page';
  $handler->subtask = 'opinion_invite_node_view';
  $handler->handler = 'http_response';
  $handler->weight = 0;
  $handler->conf = array(
    'title' => 'HTTP response code',
    'contexts' => array(),
    'relationships' => array(),
    'code' => '301',
    'destination' => 'announcements',
  );
  $page->default_handlers[$handler->name] = $handler;
  $pages['opinion_invite_node_view'] = $page;

  return $pages;

}
