<?php
/**
 * @file
 * guest_column.pages_default.inc
 */

/**
 * Implements hook_default_page_manager_handlers().
 */
function guest_column_default_page_manager_handlers() {
  $export = array();

  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'node_view_panel_context';
  $handler->task = 'node_view';
  $handler->subtask = '';
  $handler->handler = 'panel_context';
  $handler->weight = 0;
  $handler->conf = array(
    'title' => 'Guest column page',
    'no_blocks' => 0,
    'pipeline' => 'standard',
    'body_classes_to_remove' => '',
    'body_classes_to_add' => '',
    'css_id' => '',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(
      0 => array(
        'identifier' => 'Our Guest Columnist field collection (field_guest_author_info) from node (guest_column)',
        'keyword' => 'Field collection',
        'name' => 'field_collection_from_field:node:guest_column:field_guest_author_info',
        'delta' => '0',
        'context' => 'argument_entity_id:node_1',
        'id' => 1,
      ),
    ),
    'access' => array(
      'plugins' => array(
        0 => array(
          'name' => 'node_type',
          'settings' => array(
            'type' => array(
              'guest_column' => 'guest_column',
            ),
          ),
          'context' => 'argument_entity_id:node_1',
          'not' => FALSE,
        ),
      ),
      'logic' => 'and',
    ),
  );
  $display = new panels_display();
  $display->layout = 'flexible';
  $display->layout_settings = array(
    'items' => array(
      'canvas' => array(
        'type' => 'row',
        'contains' => 'column',
        'children' => array(
          0 => 'main',
        ),
        'parent' => NULL,
      ),
      'main' => array(
        'type' => 'column',
        'width' => 100,
        'width_type' => '%',
        'children' => array(
          0 => 'main-row',
        ),
        'parent' => 'canvas',
      ),
      'main-row' => array(
        'type' => 'row',
        'contains' => 'region',
        'children' => array(
          0 => 'center',
          1 => 'right',
        ),
        'parent' => 'main',
      ),
      'center' => array(
        'type' => 'region',
        'title' => 'Center',
        'width' => '74.14894445742775',
        'width_type' => '%',
        'parent' => 'main-row',
      ),
      'right' => array(
        'type' => 'region',
        'title' => 'right',
        'width' => '25.85105554257226',
        'width_type' => '%',
        'parent' => 'main-row',
        'class' => '',
      ),
    ),
  );
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'left' => NULL,
      'right' => NULL,
      'center' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = '';
  $display->uuid = '2a86c468-6c4e-4e66-b0a8-5e3b403f84ef';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-c1e09e75-8785-4e8d-a323-27bd396977d9';
    $pane->panel = 'center';
    $pane->type = 'node_type_desc';
    $pane->subtype = 'node_type_desc';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'context' => 'argument_entity_id:node_1',
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = 'c1e09e75-8785-4e8d-a323-27bd396977d9';
    $display->content['new-c1e09e75-8785-4e8d-a323-27bd396977d9'] = $pane;
    $display->panels['center'][0] = 'new-c1e09e75-8785-4e8d-a323-27bd396977d9';
    $pane = new stdClass();
    $pane->pid = 'new-c7d0f799-59cb-4698-b94a-f3b13f7fe9d9';
    $pane->panel = 'center';
    $pane->type = 'entity_field';
    $pane->subtype = 'node:field_guestcol_image';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'label' => 'title',
      'formatter' => 'image',
      'delta_limit' => 0,
      'delta_offset' => '0',
      'delta_reversed' => FALSE,
      'formatter_settings' => array(
        'image_style' => 'large',
        'image_link' => '',
      ),
      'context' => 'argument_entity_id:node_1',
      'override_title' => 1,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 1;
    $pane->locks = array();
    $pane->uuid = 'c7d0f799-59cb-4698-b94a-f3b13f7fe9d9';
    $display->content['new-c7d0f799-59cb-4698-b94a-f3b13f7fe9d9'] = $pane;
    $display->panels['center'][1] = 'new-c7d0f799-59cb-4698-b94a-f3b13f7fe9d9';
    $pane = new stdClass();
    $pane->pid = 'new-479da6de-0a80-4f8b-9c48-5302dc953266';
    $pane->panel = 'center';
    $pane->type = 'entity_field';
    $pane->subtype = 'node:body';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'label' => 'title',
      'formatter' => 'text_default',
      'delta_limit' => 0,
      'delta_offset' => '0',
      'delta_reversed' => FALSE,
      'formatter_settings' => array(),
      'context' => 'argument_entity_id:node_1',
      'override_title' => 1,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 2;
    $pane->locks = array();
    $pane->uuid = '479da6de-0a80-4f8b-9c48-5302dc953266';
    $display->content['new-479da6de-0a80-4f8b-9c48-5302dc953266'] = $pane;
    $display->panels['center'][2] = 'new-479da6de-0a80-4f8b-9c48-5302dc953266';
    $pane = new stdClass();
    $pane->pid = 'new-f16b9a99-c403-45a9-a029-ffd039071070';
    $pane->panel = 'center';
    $pane->type = 'entity_field';
    $pane->subtype = 'node:field_guest_video';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'label' => 'title',
      'formatter' => 'video_embed_field',
      'delta_limit' => 0,
      'delta_offset' => '0',
      'delta_reversed' => FALSE,
      'formatter_settings' => array(
        'video_style' => 'normal',
      ),
      'context' => 'argument_entity_id:node_1',
      'override_title' => 1,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 3;
    $pane->locks = array();
    $pane->uuid = 'f16b9a99-c403-45a9-a029-ffd039071070';
    $display->content['new-f16b9a99-c403-45a9-a029-ffd039071070'] = $pane;
    $display->panels['center'][3] = 'new-f16b9a99-c403-45a9-a029-ffd039071070';
    $pane = new stdClass();
    $pane->pid = 'new-508dbd4a-dde3-4270-89bc-267327ef6a5f';
    $pane->panel = 'center';
    $pane->type = 'entity_field';
    $pane->subtype = 'node:field_tags';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'label' => 'inline',
      'formatter' => 'taxonomy_term_reference_link',
      'delta_limit' => '0',
      'delta_offset' => '0',
      'delta_reversed' => 0,
      'formatter_settings' => array(),
      'context' => 'argument_entity_id:node_1',
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 4;
    $pane->locks = array();
    $pane->uuid = '508dbd4a-dde3-4270-89bc-267327ef6a5f';
    $display->content['new-508dbd4a-dde3-4270-89bc-267327ef6a5f'] = $pane;
    $display->panels['center'][4] = 'new-508dbd4a-dde3-4270-89bc-267327ef6a5f';
    $pane = new stdClass();
    $pane->pid = 'new-82cc3f07-8e49-4c82-a9b9-0d4f52175e93';
    $pane->panel = 'center';
    $pane->type = 'entity_field';
    $pane->subtype = 'node:field_share_this_column';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'label' => 'above',
      'formatter' => 'addthis_basic_toolbox',
      'delta_limit' => 0,
      'delta_offset' => '0',
      'delta_reversed' => FALSE,
      'formatter_settings' => array(
        'share_services' => 'email,twitter,google_plusone_share,linked in,facebook,facebook_like',
        'buttons_size' => 'addthis_32x32_style',
        'counter_orientation' => 'horizontal',
        'extra_css' => '',
      ),
      'context' => 'argument_entity_id:node_1',
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 5;
    $pane->locks = array();
    $pane->uuid = '82cc3f07-8e49-4c82-a9b9-0d4f52175e93';
    $display->content['new-82cc3f07-8e49-4c82-a9b9-0d4f52175e93'] = $pane;
    $display->panels['center'][5] = 'new-82cc3f07-8e49-4c82-a9b9-0d4f52175e93';
    $pane = new stdClass();
    $pane->pid = 'new-a262037f-5f1e-4383-a5a3-8ea2702fd42a';
    $pane->panel = 'center';
    $pane->type = 'node_comments';
    $pane->subtype = 'node_comments';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'mode' => '1',
      'comments_per_page' => '50',
      'context' => 'argument_entity_id:node_1',
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 6;
    $pane->locks = array();
    $pane->uuid = 'a262037f-5f1e-4383-a5a3-8ea2702fd42a';
    $display->content['new-a262037f-5f1e-4383-a5a3-8ea2702fd42a'] = $pane;
    $display->panels['center'][6] = 'new-a262037f-5f1e-4383-a5a3-8ea2702fd42a';
    $pane = new stdClass();
    $pane->pid = 'new-ed55b7ea-3adb-4e83-87fa-c86dee568060';
    $pane->panel = 'center';
    $pane->type = 'node_comment_form';
    $pane->subtype = 'node_comment_form';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'anon_links' => 1,
      'context' => 'argument_entity_id:node_1',
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 7;
    $pane->locks = array();
    $pane->uuid = 'ed55b7ea-3adb-4e83-87fa-c86dee568060';
    $display->content['new-ed55b7ea-3adb-4e83-87fa-c86dee568060'] = $pane;
    $display->panels['center'][7] = 'new-ed55b7ea-3adb-4e83-87fa-c86dee568060';
    $pane = new stdClass();
    $pane->pid = 'new-be845438-d7a4-48de-a824-fb2c1b16ac43';
    $pane->panel = 'right';
    $pane->type = 'custom';
    $pane->subtype = 'custom';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'admin_title' => '',
      'title' => '',
      'body' => '<h3>Our Guest Columnist</h3>',
      'format' => 'full_html',
      'substitute' => 1,
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = 'be845438-d7a4-48de-a824-fb2c1b16ac43';
    $display->content['new-be845438-d7a4-48de-a824-fb2c1b16ac43'] = $pane;
    $display->panels['right'][0] = 'new-be845438-d7a4-48de-a824-fb2c1b16ac43';
    $pane = new stdClass();
    $pane->pid = 'new-c85a4763-58aa-40d0-950a-731d01be1426';
    $pane->panel = 'right';
    $pane->type = 'entity_field';
    $pane->subtype = 'field_collection_item:field_guest_photo';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'label' => 'hidden',
      'formatter' => 'image',
      'delta_limit' => 0,
      'delta_offset' => '0',
      'delta_reversed' => FALSE,
      'formatter_settings' => array(
        'image_style' => 'medium',
        'image_link' => '',
      ),
      'context' => 'relationship_field_collection_from_field:node:guest_column:field_guest_author_info_1',
      'override_title' => 1,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 1;
    $pane->locks = array();
    $pane->uuid = 'c85a4763-58aa-40d0-950a-731d01be1426';
    $display->content['new-c85a4763-58aa-40d0-950a-731d01be1426'] = $pane;
    $display->panels['right'][1] = 'new-c85a4763-58aa-40d0-950a-731d01be1426';
    $pane = new stdClass();
    $pane->pid = 'new-646224eb-ffc3-4d8d-9e7c-969016e38819';
    $pane->panel = 'right';
    $pane->type = 'entity_field';
    $pane->subtype = 'field_collection_item:field_guest_name';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'label' => 'above',
      'formatter' => 'text_default',
      'delta_limit' => 0,
      'delta_offset' => '0',
      'delta_reversed' => FALSE,
      'formatter_settings' => array(),
      'context' => 'relationship_field_collection_from_field:node:guest_column:field_guest_author_info_1',
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 2;
    $pane->locks = array();
    $pane->uuid = '646224eb-ffc3-4d8d-9e7c-969016e38819';
    $display->content['new-646224eb-ffc3-4d8d-9e7c-969016e38819'] = $pane;
    $display->panels['right'][2] = 'new-646224eb-ffc3-4d8d-9e7c-969016e38819';
    $pane = new stdClass();
    $pane->pid = 'new-2b523267-be16-4cc4-b081-b2b8aa46fddf';
    $pane->panel = 'right';
    $pane->type = 'entity_field';
    $pane->subtype = 'field_collection_item:field_guest_qualification';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'label' => 'above',
      'formatter' => 'text_default',
      'delta_limit' => 0,
      'delta_offset' => '0',
      'delta_reversed' => FALSE,
      'formatter_settings' => array(),
      'context' => 'relationship_field_collection_from_field:node:guest_column:field_guest_author_info_1',
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 3;
    $pane->locks = array();
    $pane->uuid = '2b523267-be16-4cc4-b081-b2b8aa46fddf';
    $display->content['new-2b523267-be16-4cc4-b081-b2b8aa46fddf'] = $pane;
    $display->panels['right'][3] = 'new-2b523267-be16-4cc4-b081-b2b8aa46fddf';
    $pane = new stdClass();
    $pane->pid = 'new-53099296-5f6d-4efa-a964-55848ac193ce';
    $pane->panel = 'right';
    $pane->type = 'entity_field';
    $pane->subtype = 'field_collection_item:field_guest_bio';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'label' => 'above',
      'formatter' => 'text_default',
      'delta_limit' => 0,
      'delta_offset' => '0',
      'delta_reversed' => FALSE,
      'formatter_settings' => array(),
      'context' => 'relationship_field_collection_from_field:node:guest_column:field_guest_author_info_1',
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 4;
    $pane->locks = array();
    $pane->uuid = '53099296-5f6d-4efa-a964-55848ac193ce';
    $display->content['new-53099296-5f6d-4efa-a964-55848ac193ce'] = $pane;
    $display->panels['right'][4] = 'new-53099296-5f6d-4efa-a964-55848ac193ce';
    $pane = new stdClass();
    $pane->pid = 'new-aa2960f5-8aaa-4c58-b84b-4f2c37e1e702';
    $pane->panel = 'right';
    $pane->type = 'views';
    $pane->subtype = 'recent_guest_column_comments';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_pager_settings' => 0,
      'use_pager' => 0,
      'nodes_per_page' => '10',
      'pager_id' => '0',
      'offset' => '0',
      'more_link' => 0,
      'feed_icons' => 0,
      'panel_args' => 0,
      'link_to_view' => 0,
      'args' => '',
      'url' => '',
      'display' => 'block_1',
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 5;
    $pane->locks = array();
    $pane->uuid = 'aa2960f5-8aaa-4c58-b84b-4f2c37e1e702';
    $display->content['new-aa2960f5-8aaa-4c58-b84b-4f2c37e1e702'] = $pane;
    $display->panels['right'][5] = 'new-aa2960f5-8aaa-4c58-b84b-4f2c37e1e702';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = '0';
  $handler->conf['display'] = $display;
  $export['node_view_panel_context'] = $handler;

  return $export;
}

/**
 * Implements hook_default_page_manager_pages().
 */
function guest_column_default_page_manager_pages() {
  $page = new stdClass();
  $page->disabled = FALSE; /* Edit this to true to make a default page disabled initially */
  $page->api_version = 1;
  $page->name = 'guest_column_manage';
  $page->task = 'page';
  $page->admin_title = 'Guest column manage';
  $page->admin_description = '';
  $page->path = 'guest-columns/manage';
  $page->access = array(
    'plugins' => array(
      0 => array(
        'name' => 'perm',
        'settings' => array(
          'perm' => 'edit any guest_column content',
        ),
        'context' => 'logged-in-user',
        'not' => FALSE,
      ),
    ),
    'logic' => 'and',
    'type' => 'none',
    'settings' => NULL,
  );
  $page->menu = array(
    'type' => 'tab',
    'title' => 'Manage',
    'name' => 'navigation',
    'weight' => '5',
    'parent' => array(
      'type' => 'none',
      'title' => '',
      'name' => 'navigation',
      'weight' => '0',
    ),
  );
  $page->arguments = array();
  $page->conf = array(
    'admin_paths' => TRUE,
  );
  $page->default_handlers = array();
  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'page_guest_column_manage_menu_context';
  $handler->task = 'page';
  $handler->subtask = 'guest_column_manage';
  $handler->handler = 'menu_context';
  $handler->weight = 0;
  $handler->conf = array(
    'title' => 'Context Admin',
    'no_blocks' => FALSE,
    'css_id' => '',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
    'context_admin_options' => 'views_bulk_menu',
    'context_admin_vbo_machine_name' => 'guestcolumn_manage',
    'context_admin_vbo_views_bulk_node_type' => array(
      'guest_column' => 'guest_column',
      'page' => 0,
      'article' => 0,
      'news' => 0,
    ),
    'context_admin_vbo_views_bulk_published' => 'both',
  );
  $page->default_handlers[$handler->name] = $handler;
  $pages['guest_column_manage'] = $page;

  $page = new stdClass();
  $page->disabled = FALSE; /* Edit this to true to make a default page disabled initially */
  $page->api_version = 1;
  $page->name = 'guest_column_node_add';
  $page->task = 'page';
  $page->admin_title = 'Guest column node add';
  $page->admin_description = '';
  $page->path = 'guest-columns/add';
  $page->access = array(
    'plugins' => array(
      0 => array(
        'name' => 'perm',
        'settings' => array(
          'perm' => 'create guest_column content',
        ),
        'context' => 'logged-in-user',
        'not' => FALSE,
      ),
    ),
    'logic' => 'and',
    'type' => 'none',
    'settings' => NULL,
  );
  $page->menu = array(
    'type' => 'action',
    'title' => 'Create Expert Talk',
    'name' => 'navigation',
    'weight' => '0',
    'parent' => array(
      'type' => 'none',
      'title' => '',
      'name' => 'navigation',
      'weight' => '0',
    ),
  );
  $page->arguments = array();
  $page->conf = array(
    'admin_paths' => TRUE,
  );
  $page->default_handlers = array();
  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'page_guest_column_node_add_menu_context';
  $handler->task = 'page';
  $handler->subtask = 'guest_column_node_add';
  $handler->handler = 'menu_context';
  $handler->weight = 0;
  $handler->conf = array(
    'title' => 'Context Admin',
    'no_blocks' => FALSE,
    'css_id' => '',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
    'context_admin_options' => 'node_create_menu',
    'context_admin_options_items' => 'guest_column',
    'submitted_context' => 'empty',
    'context_admin_use_node_edit' => 1,
  );
  $page->default_handlers[$handler->name] = $handler;
  $pages['guest_column_node_add'] = $page;

  $page = new stdClass();
  $page->disabled = FALSE; /* Edit this to true to make a default page disabled initially */
  $page->api_version = 1;
  $page->name = 'guest_column_node_view';
  $page->task = 'page';
  $page->admin_title = 'Guest column node view';
  $page->admin_description = '';
  $page->path = 'guest-columns/view';
  $page->access = array(
    'type' => 'none',
    'settings' => NULL,
  );
  $page->menu = array(
    'type' => 'default tab',
    'title' => 'View',
    'name' => 'navigation',
    'weight' => '0',
    'parent' => array(
      'type' => 'none',
      'title' => '',
      'name' => 'navigation',
      'weight' => '0',
    ),
  );
  $page->arguments = array();
  $page->conf = array(
    'admin_paths' => FALSE,
  );
  $page->default_handlers = array();
  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'page_guest_column_node_view_http_response';
  $handler->task = 'page';
  $handler->subtask = 'guest_column_node_view';
  $handler->handler = 'http_response';
  $handler->weight = 0;
  $handler->conf = array(
    'title' => 'HTTP response code',
    'contexts' => array(),
    'relationships' => array(),
    'code' => '301',
    'destination' => 'guest-column',
  );
  $page->default_handlers[$handler->name] = $handler;
  $pages['guest_column_node_view'] = $page;

  return $pages;

}
