<?php
/**
 * @file
 * guest_column.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function guest_column_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'administer field collections'.
  $permissions['administer field collections'] = array(
    'name' => 'administer field collections',
    'roles' => array(
      'administrator' => 'administrator',
      'site admin' => 'site admin',
    ),
    'module' => 'field_collection',
  );

  // Exported permission: 'create guest_column content'.
  $permissions['create guest_column content'] = array(
    'name' => 'create guest_column content',
    'roles' => array(
      'administrator' => 'administrator',
      'editor' => 'editor',
      'site admin' => 'site admin',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete any guest_column content'.
  $permissions['delete any guest_column content'] = array(
    'name' => 'delete any guest_column content',
    'roles' => array(
      'administrator' => 'administrator',
      'editor' => 'editor',
      'site admin' => 'site admin',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete own guest_column content'.
  $permissions['delete own guest_column content'] = array(
    'name' => 'delete own guest_column content',
    'roles' => array(
      'administrator' => 'administrator',
      'editor' => 'editor',
      'site admin' => 'site admin',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit any guest_column content'.
  $permissions['edit any guest_column content'] = array(
    'name' => 'edit any guest_column content',
    'roles' => array(
      'administrator' => 'administrator',
      'editor' => 'editor',
      'site admin' => 'site admin',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit own guest_column content'.
  $permissions['edit own guest_column content'] = array(
    'name' => 'edit own guest_column content',
    'roles' => array(
      'administrator' => 'administrator',
      'editor' => 'editor',
      'site admin' => 'site admin',
    ),
    'module' => 'node',
  );

  // Exported permission: 'publish button publish any guest_column'.
  $permissions['publish button publish any guest_column'] = array(
    'name' => 'publish button publish any guest_column',
    'roles' => array(
      'administrator' => 'administrator',
      'editor' => 'editor',
      'site admin' => 'site admin',
    ),
    'module' => 'publish_button',
  );

  // Exported permission: 'publish button publish editable guest_column'.
  $permissions['publish button publish editable guest_column'] = array(
    'name' => 'publish button publish editable guest_column',
    'roles' => array(
      'administrator' => 'administrator',
      'editor' => 'editor',
      'site admin' => 'site admin',
    ),
    'module' => 'publish_button',
  );

  // Exported permission: 'publish button publish own guest_column'.
  $permissions['publish button publish own guest_column'] = array(
    'name' => 'publish button publish own guest_column',
    'roles' => array(
      'administrator' => 'administrator',
      'editor' => 'editor',
      'site admin' => 'site admin',
    ),
    'module' => 'publish_button',
  );

  // Exported permission: 'publish button unpublish any guest_column'.
  $permissions['publish button unpublish any guest_column'] = array(
    'name' => 'publish button unpublish any guest_column',
    'roles' => array(
      'administrator' => 'administrator',
      'editor' => 'editor',
      'site admin' => 'site admin',
    ),
    'module' => 'publish_button',
  );

  // Exported permission: 'publish button unpublish editable guest_column'.
  $permissions['publish button unpublish editable guest_column'] = array(
    'name' => 'publish button unpublish editable guest_column',
    'roles' => array(
      'administrator' => 'administrator',
      'editor' => 'editor',
      'site admin' => 'site admin',
    ),
    'module' => 'publish_button',
  );

  // Exported permission: 'publish button unpublish own guest_column'.
  $permissions['publish button unpublish own guest_column'] = array(
    'name' => 'publish button unpublish own guest_column',
    'roles' => array(
      'administrator' => 'administrator',
      'editor' => 'editor',
      'site admin' => 'site admin',
    ),
    'module' => 'publish_button',
  );

  return $permissions;
}
