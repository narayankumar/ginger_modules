<?php
/**
 * @file
 * ad.pages_default.inc
 */

/**
 * Implements hook_default_page_manager_pages().
 */
function ad_default_page_manager_pages() {
  $page = new stdClass();
  $page->disabled = FALSE; /* Edit this to true to make a default page disabled initially */
  $page->api_version = 1;
  $page->name = 'advertisement_manage';
  $page->task = 'page';
  $page->admin_title = 'Advertisement manage';
  $page->admin_description = '';
  $page->path = 'ads/manage';
  $page->access = array(
    'plugins' => array(
      0 => array(
        'name' => 'perm',
        'settings' => array(
          'perm' => 'edit any advertisement content',
        ),
        'context' => 'logged-in-user',
        'not' => FALSE,
      ),
    ),
    'logic' => 'and',
    'type' => 'none',
    'settings' => NULL,
  );
  $page->menu = array(
    'type' => 'tab',
    'title' => 'Manage',
    'name' => 'navigation',
    'weight' => '5',
    'parent' => array(
      'type' => 'none',
      'title' => '',
      'name' => 'navigation',
      'weight' => '0',
    ),
  );
  $page->arguments = array();
  $page->conf = array(
    'admin_paths' => TRUE,
  );
  $page->default_handlers = array();
  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'page_advertisement_manage_menu_context';
  $handler->task = 'page';
  $handler->subtask = 'advertisement_manage';
  $handler->handler = 'menu_context';
  $handler->weight = 0;
  $handler->conf = array(
    'title' => 'Context Admin',
    'no_blocks' => FALSE,
    'css_id' => '',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
    'context_admin_options' => 'views_bulk_menu',
    'context_admin_vbo_machine_name' => 'advertisement_manage',
    'context_admin_vbo_views_bulk_node_type' => array(
      'advertisement' => 'advertisement',
      'page' => 0,
      'article' => 0,
      'news' => 0,
      'guest_column' => 0,
      'announcement' => 0,
      'node_gallery_gallery' => 0,
      'node_gallery_item' => 0,
      'lost_pet' => 0,
      'found_pet' => 0,
      'pet_video' => 0,
      'pet_for_adoption' => 0,
      'directory_entry' => 0,
    ),
    'context_admin_vbo_views_bulk_published' => 'both',
  );
  $page->default_handlers[$handler->name] = $handler;
  $pages['advertisement_manage'] = $page;

  $page = new stdClass();
  $page->disabled = FALSE; /* Edit this to true to make a default page disabled initially */
  $page->api_version = 1;
  $page->name = 'advertisement_node_add';
  $page->task = 'page';
  $page->admin_title = 'Advertisement node add';
  $page->admin_description = '';
  $page->path = 'ads/add';
  $page->access = array(
    'plugins' => array(
      0 => array(
        'name' => 'perm',
        'settings' => array(
          'perm' => 'create advertisement content',
        ),
        'context' => 'logged-in-user',
        'not' => FALSE,
      ),
    ),
    'logic' => 'and',
    'type' => 'none',
    'settings' => NULL,
  );
  $page->menu = array(
    'type' => 'action',
    'title' => 'Create new Ad',
    'name' => 'navigation',
    'weight' => '0',
    'parent' => array(
      'type' => 'none',
      'title' => '',
      'name' => 'navigation',
      'weight' => '0',
    ),
  );
  $page->arguments = array();
  $page->conf = array(
    'admin_paths' => TRUE,
  );
  $page->default_handlers = array();
  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'page_advertisement_node_add_menu_context';
  $handler->task = 'page';
  $handler->subtask = 'advertisement_node_add';
  $handler->handler = 'menu_context';
  $handler->weight = 0;
  $handler->conf = array(
    'title' => 'Context Admin',
    'no_blocks' => FALSE,
    'css_id' => '',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
    'context_admin_options' => 'node_create_menu',
    'context_admin_options_items' => 'advertisement',
    'submitted_context' => 'empty',
    'context_admin_use_node_edit' => 1,
  );
  $page->default_handlers[$handler->name] = $handler;
  $pages['advertisement_node_add'] = $page;

  $page = new stdClass();
  $page->disabled = FALSE; /* Edit this to true to make a default page disabled initially */
  $page->api_version = 1;
  $page->name = 'advertisement_node_view';
  $page->task = 'page';
  $page->admin_title = 'Advertisement node view';
  $page->admin_description = '';
  $page->path = 'ads/view';
  $page->access = array(
    'type' => 'none',
    'settings' => NULL,
  );
  $page->menu = array(
    'type' => 'default tab',
    'title' => 'View',
    'name' => 'navigation',
    'weight' => '0',
    'parent' => array(
      'type' => 'none',
      'title' => '',
      'name' => 'navigation',
      'weight' => '0',
    ),
  );
  $page->arguments = array();
  $page->conf = array(
    'admin_paths' => FALSE,
  );
  $page->default_handlers = array();
  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'page_advertisement_node_view_http_response';
  $handler->task = 'page';
  $handler->subtask = 'advertisement_node_view';
  $handler->handler = 'http_response';
  $handler->weight = 0;
  $handler->conf = array(
    'title' => 'HTTP response code',
    'contexts' => array(),
    'relationships' => array(),
    'code' => '301',
    'destination' => 'ads',
  );
  $page->default_handlers[$handler->name] = $handler;
  $pages['advertisement_node_view'] = $page;

  return $pages;

}
