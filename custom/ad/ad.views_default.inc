<?php
/**
 * @file
 * ad.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function ad_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'ad_block_nodequeues';
  $view->description = 'Ad blocks for news, guest column, lost pet, etc.';
  $view->tag = 'nodequeue';
  $view->base_table = 'node';
  $view->human_name = 'Ad block nodequeues';
  $view->core = 0;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Defaults */
  $handler = $view->new_display('default', 'Defaults', 'default');
  $handler->display->display_options['use_more'] = TRUE;
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'none';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'none';
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['row_plugin'] = 'fields';
  /* Relationship: Nodequeue: Queue */
  $handler->display->display_options['relationships']['nodequeue_rel']['id'] = 'nodequeue_rel';
  $handler->display->display_options['relationships']['nodequeue_rel']['table'] = 'node';
  $handler->display->display_options['relationships']['nodequeue_rel']['field'] = 'nodequeue_rel';
  $handler->display->display_options['relationships']['nodequeue_rel']['required'] = TRUE;
  $handler->display->display_options['relationships']['nodequeue_rel']['limit'] = 1;
  $handler->display->display_options['relationships']['nodequeue_rel']['names'] = array(
    'ad_block_guest_column' => 'ad_block_guest_column',
    'announcement_1' => 0,
    'announcement_2' => 0,
    'announcement_3' => 0,
    'editors_photo_picks' => 0,
    'ad_blocks' => 0,
    'ad_block_2' => 0,
    'ad_block_3' => 0,
    'ad_block_news' => 0,
  );
  /* Field: Content: Type */
  $handler->display->display_options['fields']['type']['id'] = 'type';
  $handler->display->display_options['fields']['type']['table'] = 'node';
  $handler->display->display_options['fields']['type']['field'] = 'type';
  $handler->display->display_options['fields']['type']['label'] = '';
  $handler->display->display_options['fields']['type']['element_label_colon'] = FALSE;
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['element_type'] = 'h4';
  $handler->display->display_options['fields']['title']['element_label_colon'] = FALSE;
  /* Field: Content: Image */
  $handler->display->display_options['fields']['field_advt_image']['id'] = 'field_advt_image';
  $handler->display->display_options['fields']['field_advt_image']['table'] = 'field_data_field_advt_image';
  $handler->display->display_options['fields']['field_advt_image']['field'] = 'field_advt_image';
  $handler->display->display_options['fields']['field_advt_image']['label'] = '';
  $handler->display->display_options['fields']['field_advt_image']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_advt_image']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['field_advt_image']['settings'] = array(
    'image_style' => '180x120',
    'image_link' => 'content',
  );
  /* Field: Content: Text */
  $handler->display->display_options['fields']['field_advt_text']['id'] = 'field_advt_text';
  $handler->display->display_options['fields']['field_advt_text']['table'] = 'field_data_field_advt_text';
  $handler->display->display_options['fields']['field_advt_text']['field'] = 'field_advt_text';
  $handler->display->display_options['fields']['field_advt_text']['label'] = '';
  $handler->display->display_options['fields']['field_advt_text']['alter']['max_length'] = '250';
  $handler->display->display_options['fields']['field_advt_text']['alter']['ellipsis'] = FALSE;
  $handler->display->display_options['fields']['field_advt_text']['alter']['trim'] = TRUE;
  $handler->display->display_options['fields']['field_advt_text']['element_label_colon'] = FALSE;
  /* Sort criterion: Nodequeue: Position */
  $handler->display->display_options['sorts']['position']['id'] = 'position';
  $handler->display->display_options['sorts']['position']['table'] = 'nodequeue_nodes';
  $handler->display->display_options['sorts']['position']['field'] = 'position';
  $handler->display->display_options['sorts']['position']['relationship'] = 'nodequeue_rel';
  /* Filter criterion: Content: Published or admin */
  $handler->display->display_options['filters']['status_extra']['id'] = 'status_extra';
  $handler->display->display_options['filters']['status_extra']['table'] = 'node';
  $handler->display->display_options['filters']['status_extra']['field'] = 'status_extra';
  $handler->display->display_options['filters']['status_extra']['group'] = 0;
  $handler->display->display_options['filters']['status_extra']['expose']['operator'] = FALSE;

  /* Display: News Ad block */
  $handler = $view->new_display('block', 'News Ad block', 'block');
  $handler->display->display_options['defaults']['relationships'] = FALSE;
  /* Relationship: Nodequeue: Queue */
  $handler->display->display_options['relationships']['nodequeue_rel']['id'] = 'nodequeue_rel';
  $handler->display->display_options['relationships']['nodequeue_rel']['table'] = 'node';
  $handler->display->display_options['relationships']['nodequeue_rel']['field'] = 'nodequeue_rel';
  $handler->display->display_options['relationships']['nodequeue_rel']['required'] = TRUE;
  $handler->display->display_options['relationships']['nodequeue_rel']['limit'] = 1;
  $handler->display->display_options['relationships']['nodequeue_rel']['names'] = array(
    'ad_block_news' => 'ad_block_news',
    'announcement_1' => 0,
    'announcement_2' => 0,
    'announcement_3' => 0,
    'editors_photo_picks' => 0,
    'ad_blocks' => 0,
    'ad_block_2' => 0,
    'ad_block_3' => 0,
    'ad_block_guest_column' => 0,
    'ad_block_pet_videos' => 0,
  );
  $handler->display->display_options['block_description'] = 'Ad blocks - News';

  /* Display: Guest Col Ad block */
  $handler = $view->new_display('block', 'Guest Col Ad block', 'block_1');
  $handler->display->display_options['defaults']['relationships'] = FALSE;
  /* Relationship: Nodequeue: Queue */
  $handler->display->display_options['relationships']['nodequeue_rel']['id'] = 'nodequeue_rel';
  $handler->display->display_options['relationships']['nodequeue_rel']['table'] = 'node';
  $handler->display->display_options['relationships']['nodequeue_rel']['field'] = 'nodequeue_rel';
  $handler->display->display_options['relationships']['nodequeue_rel']['required'] = TRUE;
  $handler->display->display_options['relationships']['nodequeue_rel']['limit'] = 1;
  $handler->display->display_options['relationships']['nodequeue_rel']['names'] = array(
    'ad_block_guest_column' => 'ad_block_guest_column',
    'announcement_1' => 0,
    'announcement_2' => 0,
    'announcement_3' => 0,
    'editors_photo_picks' => 0,
    'ad_blocks' => 0,
    'ad_block_2' => 0,
    'ad_block_3' => 0,
    'ad_block_news' => 0,
    'ad_block_pet_videos' => 0,
  );
  $handler->display->display_options['block_description'] = 'Ad blocks - Guest Column';

  /* Display: Pet Vids Ad block */
  $handler = $view->new_display('block', 'Pet Vids Ad block', 'block_2');
  $handler->display->display_options['defaults']['relationships'] = FALSE;
  /* Relationship: Nodequeue: Queue */
  $handler->display->display_options['relationships']['nodequeue_rel']['id'] = 'nodequeue_rel';
  $handler->display->display_options['relationships']['nodequeue_rel']['table'] = 'node';
  $handler->display->display_options['relationships']['nodequeue_rel']['field'] = 'nodequeue_rel';
  $handler->display->display_options['relationships']['nodequeue_rel']['required'] = TRUE;
  $handler->display->display_options['relationships']['nodequeue_rel']['limit'] = 1;
  $handler->display->display_options['relationships']['nodequeue_rel']['names'] = array(
    'ad_block_pet_videos' => 'ad_block_pet_videos',
    'announcement_1' => 0,
    'announcement_2' => 0,
    'announcement_3' => 0,
    'editors_photo_picks' => 0,
    'ad_blocks' => 0,
    'ad_block_2' => 0,
    'ad_block_3' => 0,
    'ad_block_news' => 0,
    'ad_block_guest_column' => 0,
  );
  $handler->display->display_options['block_description'] = 'Ad blocks - Pet Videos';

  /* Display: Lost Pets Ad block */
  $handler = $view->new_display('block', 'Lost Pets Ad block', 'block_3');
  $handler->display->display_options['defaults']['relationships'] = FALSE;
  /* Relationship: Nodequeue: Queue */
  $handler->display->display_options['relationships']['nodequeue_rel']['id'] = 'nodequeue_rel';
  $handler->display->display_options['relationships']['nodequeue_rel']['table'] = 'node';
  $handler->display->display_options['relationships']['nodequeue_rel']['field'] = 'nodequeue_rel';
  $handler->display->display_options['relationships']['nodequeue_rel']['required'] = TRUE;
  $handler->display->display_options['relationships']['nodequeue_rel']['limit'] = 1;
  $handler->display->display_options['relationships']['nodequeue_rel']['names'] = array(
    'ad_block_lost_pets' => 'ad_block_lost_pets',
    'announcement_1' => 0,
    'announcement_2' => 0,
    'announcement_3' => 0,
    'editors_photo_picks' => 0,
    'ad_blocks' => 0,
    'ad_block_2' => 0,
    'ad_block_3' => 0,
    'ad_block_news' => 0,
    'ad_block_guest_column' => 0,
    'ad_block_pet_videos' => 0,
    'ad_block_found_pets' => 0,
    'ad_block_adoption' => 0,
  );
  $handler->display->display_options['block_description'] = 'Ad blocks - Lost Pets';

  /* Display: Found Pets Ad block */
  $handler = $view->new_display('block', 'Found Pets Ad block', 'block_4');
  $handler->display->display_options['defaults']['style_plugin'] = FALSE;
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['defaults']['style_options'] = FALSE;
  $handler->display->display_options['defaults']['row_plugin'] = FALSE;
  $handler->display->display_options['row_plugin'] = 'node';
  $handler->display->display_options['row_options']['view_mode'] = 'full';
  $handler->display->display_options['row_options']['links'] = FALSE;
  $handler->display->display_options['defaults']['row_options'] = FALSE;
  $handler->display->display_options['defaults']['relationships'] = FALSE;
  /* Relationship: Nodequeue: Queue */
  $handler->display->display_options['relationships']['nodequeue_rel']['id'] = 'nodequeue_rel';
  $handler->display->display_options['relationships']['nodequeue_rel']['table'] = 'node';
  $handler->display->display_options['relationships']['nodequeue_rel']['field'] = 'nodequeue_rel';
  $handler->display->display_options['relationships']['nodequeue_rel']['required'] = TRUE;
  $handler->display->display_options['relationships']['nodequeue_rel']['limit'] = 1;
  $handler->display->display_options['relationships']['nodequeue_rel']['names'] = array(
    'ad_block_found_pets' => 'ad_block_found_pets',
    'announcement_1' => 0,
    'announcement_2' => 0,
    'announcement_3' => 0,
    'editors_photo_picks' => 0,
    'ad_blocks' => 0,
    'ad_block_2' => 0,
    'ad_block_3' => 0,
    'ad_block_news' => 0,
    'ad_block_guest_column' => 0,
    'ad_block_pet_videos' => 0,
    'ad_block_lost_pets' => 0,
    'ad_block_adoption' => 0,
  );
  $handler->display->display_options['block_description'] = 'Ad blocks - Found Pets';

  /* Display: Adoption Ad block */
  $handler = $view->new_display('block', 'Adoption Ad block', 'block_5');
  $handler->display->display_options['defaults']['relationships'] = FALSE;
  /* Relationship: Nodequeue: Queue */
  $handler->display->display_options['relationships']['nodequeue_rel']['id'] = 'nodequeue_rel';
  $handler->display->display_options['relationships']['nodequeue_rel']['table'] = 'node';
  $handler->display->display_options['relationships']['nodequeue_rel']['field'] = 'nodequeue_rel';
  $handler->display->display_options['relationships']['nodequeue_rel']['required'] = TRUE;
  $handler->display->display_options['relationships']['nodequeue_rel']['limit'] = 1;
  $handler->display->display_options['relationships']['nodequeue_rel']['names'] = array(
    'ad_block_adoption' => 'ad_block_adoption',
    'announcement_1' => 0,
    'announcement_2' => 0,
    'announcement_3' => 0,
    'editors_photo_picks' => 0,
    'ad_blocks' => 0,
    'ad_block_2' => 0,
    'ad_block_3' => 0,
    'ad_block_news' => 0,
    'ad_block_guest_column' => 0,
    'ad_block_pet_videos' => 0,
    'ad_block_lost_pets' => 0,
    'ad_block_found_pets' => 0,
  );
  $handler->display->display_options['block_description'] = 'Ad blocks - Adoption';

  /* Display: Photos Ad block */
  $handler = $view->new_display('block', 'Photos Ad block', 'block_6');
  $handler->display->display_options['defaults']['relationships'] = FALSE;
  /* Relationship: Nodequeue: Queue */
  $handler->display->display_options['relationships']['nodequeue_rel']['id'] = 'nodequeue_rel';
  $handler->display->display_options['relationships']['nodequeue_rel']['table'] = 'node';
  $handler->display->display_options['relationships']['nodequeue_rel']['field'] = 'nodequeue_rel';
  $handler->display->display_options['relationships']['nodequeue_rel']['required'] = TRUE;
  $handler->display->display_options['relationships']['nodequeue_rel']['limit'] = 1;
  $handler->display->display_options['relationships']['nodequeue_rel']['names'] = array(
    'ad_block_photos' => 'ad_block_photos',
    'announcement_1' => 0,
    'announcement_2' => 0,
    'announcement_3' => 0,
    'editors_photo_picks' => 0,
    'ad_blocks' => 0,
    'ad_block_2' => 0,
    'ad_block_3' => 0,
    'ad_block_news' => 0,
    'ad_block_guest_column' => 0,
    'ad_block_pet_videos' => 0,
    'ad_block_lost_pets' => 0,
    'ad_block_found_pets' => 0,
    'ad_block_adoption' => 0,
  );
  $handler->display->display_options['block_description'] = 'Ad blocks - Photos';

  /* Display: Articles Ad block */
  $handler = $view->new_display('block', 'Articles Ad block', 'block_7');
  $handler->display->display_options['defaults']['relationships'] = FALSE;
  /* Relationship: Nodequeue: Queue */
  $handler->display->display_options['relationships']['nodequeue_rel']['id'] = 'nodequeue_rel';
  $handler->display->display_options['relationships']['nodequeue_rel']['table'] = 'node';
  $handler->display->display_options['relationships']['nodequeue_rel']['field'] = 'nodequeue_rel';
  $handler->display->display_options['relationships']['nodequeue_rel']['required'] = TRUE;
  $handler->display->display_options['relationships']['nodequeue_rel']['limit'] = 1;
  $handler->display->display_options['relationships']['nodequeue_rel']['names'] = array(
    'ad_block_articles' => 'ad_block_articles',
    'announcement_1' => 0,
    'announcement_2' => 0,
    'announcement_3' => 0,
    'editors_photo_picks' => 0,
    'ad_block_news' => 0,
    'ad_block_guest_column' => 0,
    'ad_block_pet_videos' => 0,
    'ad_block_lost_pets' => 0,
    'ad_block_found_pets' => 0,
    'ad_block_adoption' => 0,
    'ad_block_photos' => 0,
    'ad_block_front_page' => 0,
    'ad_block_directory' => 0,
  );
  $handler->display->display_options['block_description'] = 'Ad blocks - Articles';

  /* Display: Directory Ad block */
  $handler = $view->new_display('block', 'Directory Ad block', 'block_8');
  $handler->display->display_options['defaults']['relationships'] = FALSE;
  /* Relationship: Nodequeue: Queue */
  $handler->display->display_options['relationships']['nodequeue_rel']['id'] = 'nodequeue_rel';
  $handler->display->display_options['relationships']['nodequeue_rel']['table'] = 'node';
  $handler->display->display_options['relationships']['nodequeue_rel']['field'] = 'nodequeue_rel';
  $handler->display->display_options['relationships']['nodequeue_rel']['required'] = TRUE;
  $handler->display->display_options['relationships']['nodequeue_rel']['limit'] = 1;
  $handler->display->display_options['relationships']['nodequeue_rel']['names'] = array(
    'ad_block_directory' => 'ad_block_directory',
    'announcement_1' => 0,
    'announcement_2' => 0,
    'announcement_3' => 0,
    'editors_photo_picks' => 0,
    'ad_block_news' => 0,
    'ad_block_guest_column' => 0,
    'ad_block_pet_videos' => 0,
    'ad_block_lost_pets' => 0,
    'ad_block_found_pets' => 0,
    'ad_block_adoption' => 0,
    'ad_block_photos' => 0,
    'ad_block_articles' => 0,
    'ad_block_front_page' => 0,
  );
  $handler->display->display_options['block_description'] = 'Ad blocks - Directory';

  /* Display: Front Page Ad block */
  $handler = $view->new_display('block', 'Front Page Ad block', 'block_9');
  $handler->display->display_options['defaults']['relationships'] = FALSE;
  /* Relationship: Nodequeue: Queue */
  $handler->display->display_options['relationships']['nodequeue_rel']['id'] = 'nodequeue_rel';
  $handler->display->display_options['relationships']['nodequeue_rel']['table'] = 'node';
  $handler->display->display_options['relationships']['nodequeue_rel']['field'] = 'nodequeue_rel';
  $handler->display->display_options['relationships']['nodequeue_rel']['required'] = TRUE;
  $handler->display->display_options['relationships']['nodequeue_rel']['limit'] = 1;
  $handler->display->display_options['relationships']['nodequeue_rel']['names'] = array(
    'ad_block_front_page' => 'ad_block_front_page',
    'announcement_1' => 0,
    'announcement_2' => 0,
    'announcement_3' => 0,
    'editors_photo_picks' => 0,
    'ad_block_news' => 0,
    'ad_block_guest_column' => 0,
    'ad_block_pet_videos' => 0,
    'ad_block_lost_pets' => 0,
    'ad_block_found_pets' => 0,
    'ad_block_adoption' => 0,
    'ad_block_photos' => 0,
    'ad_block_articles' => 0,
    'ad_block_directory' => 0,
  );
  $handler->display->display_options['block_description'] = 'Ad blocks - Front Page';

  /* Display: Lost-found page block */
  $handler = $view->new_display('block', 'Lost-found page block', 'block_10');
  $handler->display->display_options['defaults']['relationships'] = FALSE;
  /* Relationship: Nodequeue: Queue */
  $handler->display->display_options['relationships']['nodequeue_rel']['id'] = 'nodequeue_rel';
  $handler->display->display_options['relationships']['nodequeue_rel']['table'] = 'node';
  $handler->display->display_options['relationships']['nodequeue_rel']['field'] = 'nodequeue_rel';
  $handler->display->display_options['relationships']['nodequeue_rel']['required'] = TRUE;
  $handler->display->display_options['relationships']['nodequeue_rel']['limit'] = 1;
  $handler->display->display_options['relationships']['nodequeue_rel']['names'] = array(
    'ad_blocks_lost_found_page' => 'ad_blocks_lost_found_page',
    'announcement_1' => 0,
    'announcement_2' => 0,
    'announcement_3' => 0,
    'editors_photo_picks' => 0,
    'ad_block_news' => 0,
    'ad_block_guest_column' => 0,
    'ad_block_pet_videos' => 0,
    'ad_block_lost_pets' => 0,
    'ad_block_found_pets' => 0,
    'ad_block_adoption' => 0,
    'ad_block_photos' => 0,
    'ad_block_articles' => 0,
    'ad_block_front_page' => 0,
    'ad_block_directory' => 0,
  );
  $handler->display->display_options['block_description'] = 'Ad blocks - Lost-Found Page';
  $export['ad_block_nodequeues'] = $view;

  $view = new view();
  $view->name = 'ad_teasers_page';
  $view->description = 'Page listing Guest column teasers';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'Ad teasers page';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Advertisements';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['access']['perm'] = 'edit any advertisement content';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '10';
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['row_plugin'] = 'node';
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
  /* Sort criterion: Content: Post date */
  $handler->display->display_options['sorts']['created']['id'] = 'created';
  $handler->display->display_options['sorts']['created']['table'] = 'node';
  $handler->display->display_options['sorts']['created']['field'] = 'created';
  $handler->display->display_options['sorts']['created']['order'] = 'DESC';
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'article' => 'article',
  );

  /* Display: Ad teasers for admin view */
  $handler = $view->new_display('page', 'Ad teasers for admin view', 'page');
  $handler->display->display_options['defaults']['pager'] = FALSE;
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '24';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['pager']['options']['id'] = '0';
  $handler->display->display_options['pager']['options']['quantity'] = '9';
  $handler->display->display_options['defaults']['style_plugin'] = FALSE;
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['style_options']['row_class'] = 'row album-row';
  $handler->display->display_options['defaults']['style_options'] = FALSE;
  $handler->display->display_options['defaults']['row_plugin'] = FALSE;
  $handler->display->display_options['row_plugin'] = 'fields';
  $handler->display->display_options['row_options']['inline'] = array(
    'field_pet_city' => 'field_pet_city',
  );
  $handler->display->display_options['row_options']['separator'] = ', ';
  $handler->display->display_options['defaults']['row_options'] = FALSE;
  $handler->display->display_options['defaults']['fields'] = FALSE;
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['alter']['max_length'] = '20';
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['title']['alter']['trim'] = TRUE;
  $handler->display->display_options['fields']['title']['element_type'] = 'span';
  $handler->display->display_options['fields']['title']['element_class'] = 'thumb-info-title';
  $handler->display->display_options['fields']['title']['element_label_colon'] = FALSE;
  /* Field: Content: Image */
  $handler->display->display_options['fields']['field_advt_image']['id'] = 'field_advt_image';
  $handler->display->display_options['fields']['field_advt_image']['table'] = 'field_data_field_advt_image';
  $handler->display->display_options['fields']['field_advt_image']['field'] = 'field_advt_image';
  $handler->display->display_options['fields']['field_advt_image']['label'] = '';
  $handler->display->display_options['fields']['field_advt_image']['element_type'] = 'div';
  $handler->display->display_options['fields']['field_advt_image']['element_class'] = 'post-image img-thumbnail';
  $handler->display->display_options['fields']['field_advt_image']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_advt_image']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['field_advt_image']['settings'] = array(
    'image_style' => 'node_gallery_thumbnail',
    'image_link' => 'content',
  );
  $handler->display->display_options['defaults']['filter_groups'] = FALSE;
  $handler->display->display_options['defaults']['filters'] = FALSE;
  /* Filter criterion: Content: Published or admin */
  $handler->display->display_options['filters']['status_extra']['id'] = 'status_extra';
  $handler->display->display_options['filters']['status_extra']['table'] = 'node';
  $handler->display->display_options['filters']['status_extra']['field'] = 'status_extra';
  $handler->display->display_options['filters']['status_extra']['group'] = 1;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'advertisement' => 'advertisement',
  );
  $handler->display->display_options['filters']['type']['group'] = 1;
  $handler->display->display_options['path'] = 'ads';
  $handler->display->display_options['menu']['type'] = 'normal';
  $handler->display->display_options['menu']['title'] = 'Ads';
  $handler->display->display_options['menu']['weight'] = '4';
  $handler->display->display_options['menu']['name'] = 'user-menu';
  $handler->display->display_options['menu']['context'] = 0;
  $handler->display->display_options['menu']['context_only_inline'] = 0;
  $export['ad_teasers_page'] = $view;

  $view = new view();
  $view->name = 'directory_ratings_of_friends';
  $view->description = 'Ratings on directory entries by users I follow';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'DIrectory ratings of friends';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Ratings of friends I follow';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['row_plugin'] = 'fields';
  $handler->display->display_options['row_options']['inline'] = array(
    'value' => 'value',
  );
  /* Relationship: Content: Votes */
  $handler->display->display_options['relationships']['votingapi_vote']['id'] = 'votingapi_vote';
  $handler->display->display_options['relationships']['votingapi_vote']['table'] = 'node';
  $handler->display->display_options['relationships']['votingapi_vote']['field'] = 'votingapi_vote';
  $handler->display->display_options['relationships']['votingapi_vote']['required'] = TRUE;
  $handler->display->display_options['relationships']['votingapi_vote']['votingapi'] = array(
    'value_type' => 'percent',
    'tag' => 'vote',
  );
  $handler->display->display_options['relationships']['votingapi_vote']['current_user'] = 0;
  /* Relationship: Votes: User */
  $handler->display->display_options['relationships']['uid']['id'] = 'uid';
  $handler->display->display_options['relationships']['uid']['table'] = 'votingapi_vote';
  $handler->display->display_options['relationships']['uid']['field'] = 'uid';
  $handler->display->display_options['relationships']['uid']['relationship'] = 'votingapi_vote';
  $handler->display->display_options['relationships']['uid']['label'] = 'Voter';
  /* Relationship: Flags: follow_user */
  $handler->display->display_options['relationships']['flag_content_rel']['id'] = 'flag_content_rel';
  $handler->display->display_options['relationships']['flag_content_rel']['table'] = 'users';
  $handler->display->display_options['relationships']['flag_content_rel']['field'] = 'flag_content_rel';
  $handler->display->display_options['relationships']['flag_content_rel']['relationship'] = 'uid';
  $handler->display->display_options['relationships']['flag_content_rel']['flag'] = 'follow_user';
  /* Field: User: Name */
  $handler->display->display_options['fields']['name']['id'] = 'name';
  $handler->display->display_options['fields']['name']['table'] = 'users';
  $handler->display->display_options['fields']['name']['field'] = 'name';
  $handler->display->display_options['fields']['name']['relationship'] = 'uid';
  $handler->display->display_options['fields']['name']['label'] = '';
  $handler->display->display_options['fields']['name']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['name']['alter']['text'] = 'Rated by: [name]';
  $handler->display->display_options['fields']['name']['element_label_colon'] = FALSE;
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = 'Entry';
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
  /* Field: Votes: Value */
  $handler->display->display_options['fields']['value']['id'] = 'value';
  $handler->display->display_options['fields']['value']['table'] = 'votingapi_vote';
  $handler->display->display_options['fields']['value']['field'] = 'value';
  $handler->display->display_options['fields']['value']['relationship'] = 'votingapi_vote';
  $handler->display->display_options['fields']['value']['label'] = 'Rating';
  $handler->display->display_options['fields']['value']['precision'] = '0';
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'directory_entry' => 'directory_entry',
  );

  /* Display: Block */
  $handler = $view->new_display('block', 'Block', 'block_1');
  $export['directory_ratings_of_friends'] = $view;

  return $export;
}
