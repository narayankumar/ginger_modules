<?php
/**
 * @file
 * ad.features.field_instance.inc
 */

/**
 * Implements hook_field_default_field_instances().
 */
function ad_field_default_field_instances() {
  $field_instances = array();

  // Exported field_instance: 'node-advertisement-field_advt_image'
  $field_instances['node-advertisement-field_advt_image'] = array(
    'bundle' => 'advertisement',
    'deleted' => 0,
    'description' => 'Upload a visual for the ad',
    'display' => array(
      'default' => array(
        'label' => 'hidden',
        'module' => 'image',
        'settings' => array(
          'image_link' => 'content',
          'image_style' => '180x120',
        ),
        'type' => 'image',
        'weight' => 0,
      ),
      'node_gallery_node_thumbnail' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
      'teaser' => array(
        'label' => 'hidden',
        'module' => 'image',
        'settings' => array(
          'image_link' => 'content',
          'image_style' => '180x120',
        ),
        'type' => 'image',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_advt_image',
    'label' => 'Image',
    'required' => 0,
    'settings' => array(
      'alt_field' => 0,
      'default_image' => 0,
      'file_directory' => 'field/ads',
      'file_extensions' => 'png gif jpg jpeg',
      'max_filesize' => '2 MB',
      'max_resolution' => '800x800',
      'min_resolution' => '',
      'title_field' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'image',
      'settings' => array(
        'preview_image_style' => 'thumbnail',
        'progress_indicator' => 'throbber',
      ),
      'type' => 'image_image',
      'weight' => 1,
    ),
  );

  // Exported field_instance: 'node-advertisement-field_advt_text'
  $field_instances['node-advertisement-field_advt_text'] = array(
    'bundle' => 'advertisement',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => 'Only the first 250 characters will be displayed.',
    'display' => array(
      'default' => array(
        'label' => 'hidden',
        'module' => 'text',
        'settings' => array(
          'trim_length' => 250,
        ),
        'type' => 'text_trimmed',
        'weight' => 1,
      ),
      'node_gallery_node_thumbnail' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
      'teaser' => array(
        'label' => 'hidden',
        'module' => 'text',
        'settings' => array(
          'trim_length' => 250,
        ),
        'type' => 'text_trimmed',
        'weight' => 1,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_advt_text',
    'label' => 'Text',
    'required' => 0,
    'settings' => array(
      'text_processing' => 1,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'rows' => 3,
      ),
      'type' => 'text_textarea',
      'weight' => 2,
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Image');
  t('Only the first 250 characters will be displayed.');
  t('Text');
  t('Upload a visual for the ad');

  return $field_instances;
}
