<?php
/**
 * @file
 * ad.context.inc
 */

/**
 * Implements hook_context_default_contexts().
 */
function ad_context_default_contexts() {
  $export = array();

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'directory_blocks';
  $context->description = 'blocks on directory page';
  $context->tag = '';
  $context->conditions = array(
    'node' => array(
      'values' => array(
        'directory_entry' => 'directory_entry',
      ),
      'options' => array(
        'node_form' => '1',
      ),
    ),
    'views' => array(
      'values' => array(
        'directory_teasers_page' => 'directory_teasers_page',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'views-ad_block_nodequeues-block_8' => array(
          'module' => 'views',
          'delta' => 'ad_block_nodequeues-block_8',
          'region' => 'sidebar_second',
          'weight' => '-10',
        ),
        'views-db4564942d17643fbfba2ee40227d3c4' => array(
          'module' => 'views',
          'delta' => 'db4564942d17643fbfba2ee40227d3c4',
          'region' => 'sidebar_second',
          'weight' => '-8',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('blocks on directory page');
  $export['directory_blocks'] = $context;

  return $export;
}
