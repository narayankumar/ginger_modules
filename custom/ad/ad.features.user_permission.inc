<?php
/**
 * @file
 * ad.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function ad_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'create advertisement content'.
  $permissions['create advertisement content'] = array(
    'name' => 'create advertisement content',
    'roles' => array(
      'administrator' => 'administrator',
      'editor' => 'editor',
      'site admin' => 'site admin',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete any advertisement content'.
  $permissions['delete any advertisement content'] = array(
    'name' => 'delete any advertisement content',
    'roles' => array(
      'administrator' => 'administrator',
      'editor' => 'editor',
      'site admin' => 'site admin',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete own advertisement content'.
  $permissions['delete own advertisement content'] = array(
    'name' => 'delete own advertisement content',
    'roles' => array(
      'administrator' => 'administrator',
      'editor' => 'editor',
      'site admin' => 'site admin',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit any advertisement content'.
  $permissions['edit any advertisement content'] = array(
    'name' => 'edit any advertisement content',
    'roles' => array(
      'administrator' => 'administrator',
      'editor' => 'editor',
      'site admin' => 'site admin',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit own advertisement content'.
  $permissions['edit own advertisement content'] = array(
    'name' => 'edit own advertisement content',
    'roles' => array(
      'administrator' => 'administrator',
      'editor' => 'editor',
      'site admin' => 'site admin',
    ),
    'module' => 'node',
  );

  // Exported permission: 'publish button publish any advertisement'.
  $permissions['publish button publish any advertisement'] = array(
    'name' => 'publish button publish any advertisement',
    'roles' => array(
      'administrator' => 'administrator',
      'editor' => 'editor',
      'site admin' => 'site admin',
    ),
    'module' => 'publish_button',
  );

  // Exported permission: 'publish button publish editable advertisement'.
  $permissions['publish button publish editable advertisement'] = array(
    'name' => 'publish button publish editable advertisement',
    'roles' => array(
      'administrator' => 'administrator',
      'editor' => 'editor',
      'site admin' => 'site admin',
    ),
    'module' => 'publish_button',
  );

  // Exported permission: 'publish button publish own advertisement'.
  $permissions['publish button publish own advertisement'] = array(
    'name' => 'publish button publish own advertisement',
    'roles' => array(
      'administrator' => 'administrator',
      'editor' => 'editor',
      'site admin' => 'site admin',
    ),
    'module' => 'publish_button',
  );

  // Exported permission: 'publish button unpublish any advertisement'.
  $permissions['publish button unpublish any advertisement'] = array(
    'name' => 'publish button unpublish any advertisement',
    'roles' => array(
      'administrator' => 'administrator',
      'editor' => 'editor',
      'site admin' => 'site admin',
    ),
    'module' => 'publish_button',
  );

  // Exported permission: 'publish button unpublish editable advertisement'.
  $permissions['publish button unpublish editable advertisement'] = array(
    'name' => 'publish button unpublish editable advertisement',
    'roles' => array(
      'administrator' => 'administrator',
      'editor' => 'editor',
      'site admin' => 'site admin',
    ),
    'module' => 'publish_button',
  );

  // Exported permission: 'publish button unpublish own advertisement'.
  $permissions['publish button unpublish own advertisement'] = array(
    'name' => 'publish button unpublish own advertisement',
    'roles' => array(
      'administrator' => 'administrator',
      'editor' => 'editor',
      'site admin' => 'site admin',
    ),
    'module' => 'publish_button',
  );

  return $permissions;
}
