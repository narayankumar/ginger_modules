<?php
/**
 * @file
 * lost_pet.context.inc
 */

/**
 * Implements hook_context_default_contexts().
 */
function lost_pet_context_default_contexts() {
  $export = array();

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'lost_pets_ad_blocks';
  $context->description = 'Ad bllocks on lost pets nodes and teasers page';
  $context->tag = '';
  $context->conditions = array(
    'node' => array(
      'values' => array(
        'lost_pet' => 'lost_pet',
      ),
      'options' => array(
        'node_form' => '1',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'views-ad_block_nodequeues-block_3' => array(
          'module' => 'views',
          'delta' => 'ad_block_nodequeues-block_3',
          'region' => 'sidebar_second',
          'weight' => '-10',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Ad bllocks on lost pets nodes and teasers page');
  $export['lost_pets_ad_blocks'] = $context;

  return $export;
}
