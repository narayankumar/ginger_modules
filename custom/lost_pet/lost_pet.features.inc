<?php
/**
 * @file
 * lost_pet.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function lost_pet_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "context" && $api == "context") {
    return array("version" => "3");
  }
  if ($module == "page_manager" && $api == "pages_default") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function lost_pet_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_fe_nodequeue_export_fields().
 */
function lost_pet_fe_nodequeue_export_fields() {
  $nodequeues = array();

  // Exported nodequeues: ad_block_lost_pets
  $nodequeues['ad_block_lost_pets'] = array(
    'name' => 'ad_block_lost_pets',
    'title' => 'Ad block - Lost pets',
    'subqueue_title' => '',
    'size' => 4,
    'link' => '',
    'link_remove' => '',
    'owner' => 'nodequeue',
    'show_in_ui' => 1,
    'show_in_tab' => 1,
    'show_in_links' => 0,
    'reference' => 0,
    'reverse' => 0,
    'i18n' => 0,
    'subqueues' => 1,
    'types' => array(
      0 => 'advertisement',
    ),
    'roles' => array(),
    'count' => 0,
  );

  return $nodequeues;
}

/**
 * Implements hook_flag_default_flags().
 */
function lost_pet_flag_default_flags() {
  $flags = array();
  // Exported flag: "Pet Found".
  $flags['pet_found'] = array(
    'entity_type' => 'node',
    'title' => 'Pet Found',
    'global' => 1,
    'types' => array(
      0 => 'lost_pet',
    ),
    'flag_short' => 'Pet has been found',
    'flag_long' => '',
    'flag_message' => '',
    'unflag_short' => 'Cancel \'found pet\'',
    'unflag_long' => '',
    'unflag_message' => '',
    'unflag_denied_text' => 'Found!',
    'link_type' => 'confirm',
    'weight' => 0,
    'show_in_links' => array(
      'full' => 'full',
      'teaser' => 'teaser',
      'rss' => 0,
      'search_index' => 0,
      'search_result' => 0,
      'diff_standard' => 0,
      'node_gallery_node_thumbnail' => 0,
      'token' => 0,
    ),
    'show_as_field' => 0,
    'show_on_form' => 0,
    'access_author' => 'own',
    'show_contextual_link' => 0,
    'i18n' => 0,
    'flag_confirmation' => 'Are you sure you want to mark this as \'FOUND\'?',
    'unflag_confirmation' => 'Are you sure you want to cancel the \'FOUND\' status?',
    'module' => 'lost_pet',
    'locked' => array(
      0 => 'name',
    ),
    'api_version' => 3,
  );
  return $flags;

}

/**
 * Implements hook_node_info().
 */
function lost_pet_node_info() {
  $items = array(
    'lost_pet' => array(
      'name' => t('Lost Pet'),
      'base' => 'node_content',
      'description' => t('Report details of a lost pet with this'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  return $items;
}
