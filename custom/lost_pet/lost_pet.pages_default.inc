<?php
/**
 * @file
 * lost_pet.pages_default.inc
 */

/**
 * Implements hook_default_page_manager_pages().
 */
function lost_pet_default_page_manager_pages() {
  $page = new stdClass();
  $page->disabled = FALSE; /* Edit this to true to make a default page disabled initially */
  $page->api_version = 1;
  $page->name = 'cities_manage_tab';
  $page->task = 'page';
  $page->admin_title = 'Cities manage tab';
  $page->admin_description = 'Used for managing City taxonomy list for Lost Pet CT';
  $page->path = 'lost-pet/city-list';
  $page->access = array(
    'plugins' => array(
      0 => array(
        'name' => 'perm',
        'settings' => array(
          'perm' => 'edit any lost_pet content',
        ),
        'context' => 'logged-in-user',
        'not' => FALSE,
      ),
    ),
    'logic' => 'and',
  );
  $page->menu = array(
    'type' => 'tab',
    'title' => 'List Cities',
    'name' => 'navigation',
    'weight' => '2',
    'parent' => array(
      'type' => 'tab',
      'title' => 'City',
      'name' => 'navigation',
      'weight' => '0',
    ),
  );
  $page->arguments = array();
  $page->conf = array(
    'admin_paths' => FALSE,
  );
  $page->default_handlers = array();
  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'page_cities_manage_tab_menu_context';
  $handler->task = 'page';
  $handler->subtask = 'cities_manage_tab';
  $handler->handler = 'menu_context';
  $handler->weight = 0;
  $handler->conf = array(
    'title' => 'Context Admin',
    'no_blocks' => FALSE,
    'css_id' => '',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
    'context_admin_options' => 'taxonomy_list_menu',
    'context_admin_vocabulary' => 'cities',
    'context_admin_vocabulary_options' => 'list',
  );
  $page->default_handlers[$handler->name] = $handler;
  $pages['cities_manage_tab'] = $page;

  $page = new stdClass();
  $page->disabled = FALSE; /* Edit this to true to make a default page disabled initially */
  $page->api_version = 1;
  $page->name = 'city_add_tab';
  $page->task = 'page';
  $page->admin_title = 'City add tab';
  $page->admin_description = 'Used for managing City taxonomy list for Lost Pet CT';
  $page->path = 'lost-pet/city-add';
  $page->access = array(
    'plugins' => array(
      0 => array(
        'name' => 'perm',
        'settings' => array(
          'perm' => 'edit any lost_pet content',
        ),
        'context' => 'logged-in-user',
        'not' => FALSE,
      ),
    ),
    'logic' => 'and',
  );
  $page->menu = array(
    'type' => 'tab',
    'title' => 'Add City',
    'name' => 'navigation',
    'weight' => '3',
    'parent' => array(
      'type' => 'tab',
      'title' => 'City',
      'name' => 'navigation',
      'weight' => '0',
    ),
  );
  $page->arguments = array();
  $page->conf = array(
    'admin_paths' => FALSE,
  );
  $page->default_handlers = array();
  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'page_city_add_tab_menu_context';
  $handler->task = 'page';
  $handler->subtask = 'city_add_tab';
  $handler->handler = 'menu_context';
  $handler->weight = 0;
  $handler->conf = array(
    'title' => 'Context Admin',
    'no_blocks' => FALSE,
    'css_id' => '',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
    'context_admin_options' => 'taxonomy_list_menu',
    'context_admin_vocabulary' => 'cities',
    'context_admin_vocabulary_options' => 'add',
  );
  $page->default_handlers[$handler->name] = $handler;
  $pages['city_add_tab'] = $page;

  $page = new stdClass();
  $page->disabled = FALSE; /* Edit this to true to make a default page disabled initially */
  $page->api_version = 1;
  $page->name = 'lost_and_found_page';
  $page->task = 'page';
  $page->admin_title = 'Lost and found page';
  $page->admin_description = 'display page for lost pets and found pets, divided vertically';
  $page->path = 'lost-found';
  $page->access = array();
  $page->menu = array(
    'type' => 'normal',
    'title' => 'Lost & Found',
    'name' => 'navigation',
    'weight' => '2',
    'parent' => array(
      'type' => 'none',
      'title' => '',
      'name' => 'navigation',
      'weight' => '0',
    ),
  );
  $page->arguments = array();
  $page->conf = array(
    'admin_paths' => FALSE,
  );
  $page->default_handlers = array();
  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'page_lost_and_found_page_panel_context';
  $handler->task = 'page';
  $handler->subtask = 'lost_and_found_page';
  $handler->handler = 'panel_context';
  $handler->weight = 0;
  $handler->conf = array(
    'title' => 'Lost and Found Panel',
    'no_blocks' => 0,
    'pipeline' => 'standard',
    'body_classes_to_remove' => '',
    'body_classes_to_add' => '',
    'css_id' => '',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
  );
  $display = new panels_display();
  $display->layout = 'twocol';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'left' => NULL,
      'right' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = '';
  $display->uuid = '1768b1cd-40f8-42f9-bf09-6c1315ce83e2';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-2873a72e-cdef-46f8-a625-e34d377a577f';
    $pane->panel = 'left';
    $pane->type = 'custom';
    $pane->subtype = 'custom';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'admin_title' => 'Links for lost pet on lost-found page',
      'title' => '',
      'body' => '<p><a href="../lost-pet">+ Lost your pet?</a></p>',
      'format' => 'full_html',
      'substitute' => TRUE,
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array(
      'css_id' => '',
      'css_class' => 'create-lost-found',
    );
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '2873a72e-cdef-46f8-a625-e34d377a577f';
    $display->content['new-2873a72e-cdef-46f8-a625-e34d377a577f'] = $pane;
    $display->panels['left'][0] = 'new-2873a72e-cdef-46f8-a625-e34d377a577f';
    $pane = new stdClass();
    $pane->pid = 'new-e08d679b-b4bb-4d6a-bc42-132fc0a307f5';
    $pane->panel = 'left';
    $pane->type = 'views';
    $pane->subtype = 'lost_pet_teasers_page';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_pager_settings' => 0,
      'use_pager' => 1,
      'nodes_per_page' => '24',
      'pager_id' => '0',
      'offset' => '0',
      'more_link' => 0,
      'feed_icons' => 0,
      'panel_args' => 0,
      'link_to_view' => 0,
      'args' => '',
      'url' => '',
      'display' => 'page',
      'override_title' => 1,
      'override_title_text' => '<h4 class="black">%title</h4>',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 1;
    $pane->locks = array();
    $pane->uuid = 'e08d679b-b4bb-4d6a-bc42-132fc0a307f5';
    $display->content['new-e08d679b-b4bb-4d6a-bc42-132fc0a307f5'] = $pane;
    $display->panels['left'][1] = 'new-e08d679b-b4bb-4d6a-bc42-132fc0a307f5';
    $pane = new stdClass();
    $pane->pid = 'new-71aae82a-c527-4e70-97bb-f785e044eb6f';
    $pane->panel = 'right';
    $pane->type = 'custom';
    $pane->subtype = 'custom';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'admin_title' => 'Found pet link on lost-found page',
      'title' => '',
      'body' => '<p><a href="../found-pet">+ Found a pet?</a></p>',
      'format' => 'full_html',
      'substitute' => TRUE,
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array(
      'css_id' => '',
      'css_class' => 'create-lost-found',
    );
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '71aae82a-c527-4e70-97bb-f785e044eb6f';
    $display->content['new-71aae82a-c527-4e70-97bb-f785e044eb6f'] = $pane;
    $display->panels['right'][0] = 'new-71aae82a-c527-4e70-97bb-f785e044eb6f';
    $pane = new stdClass();
    $pane->pid = 'new-2cdf8aad-f5f2-4663-973f-683fc1a60a46';
    $pane->panel = 'right';
    $pane->type = 'views';
    $pane->subtype = 'found_pet_teasers_page';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_pager_settings' => 0,
      'use_pager' => 1,
      'nodes_per_page' => '24',
      'pager_id' => '0',
      'offset' => '0',
      'more_link' => 0,
      'feed_icons' => 0,
      'panel_args' => 0,
      'link_to_view' => 0,
      'args' => '',
      'url' => '',
      'display' => 'page',
      'override_title' => 1,
      'override_title_text' => '<h4 class="black">Found pets</h4>',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 1;
    $pane->locks = array();
    $pane->uuid = '2cdf8aad-f5f2-4663-973f-683fc1a60a46';
    $display->content['new-2cdf8aad-f5f2-4663-973f-683fc1a60a46'] = $pane;
    $display->panels['right'][1] = 'new-2cdf8aad-f5f2-4663-973f-683fc1a60a46';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = '0';
  $handler->conf['display'] = $display;
  $page->default_handlers[$handler->name] = $handler;
  $pages['lost_and_found_page'] = $page;

  $page = new stdClass();
  $page->disabled = FALSE; /* Edit this to true to make a default page disabled initially */
  $page->api_version = 1;
  $page->name = 'lost_pet_manage';
  $page->task = 'page';
  $page->admin_title = 'Lost pet manage';
  $page->admin_description = '';
  $page->path = 'lost-pet/manage';
  $page->access = array(
    'plugins' => array(
      0 => array(
        'name' => 'perm',
        'settings' => array(
          'perm' => 'edit any lost_pet content',
        ),
        'context' => 'logged-in-user',
        'not' => FALSE,
      ),
    ),
    'logic' => 'and',
    'type' => 'none',
    'settings' => NULL,
  );
  $page->menu = array(
    'type' => 'tab',
    'title' => 'Manage',
    'name' => 'navigation',
    'weight' => '1',
    'parent' => array(
      'type' => 'none',
      'title' => '',
      'name' => 'navigation',
      'weight' => '0',
    ),
  );
  $page->arguments = array();
  $page->conf = array(
    'admin_paths' => TRUE,
  );
  $page->default_handlers = array();
  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'page_lost_pet_manage_menu_context';
  $handler->task = 'page';
  $handler->subtask = 'lost_pet_manage';
  $handler->handler = 'menu_context';
  $handler->weight = 0;
  $handler->conf = array(
    'title' => 'Context Admin',
    'no_blocks' => FALSE,
    'css_id' => '',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
    'context_admin_options' => 'views_bulk_menu',
    'context_admin_vbo_machine_name' => 'lostpet_manage',
    'context_admin_vbo_views_bulk_node_type' => array(
      'lost_pet' => 'lost_pet',
      'page' => 0,
      'article' => 0,
      'news' => 0,
      'guest_column' => 0,
      'announcement' => 0,
      'node_gallery_gallery' => 0,
      'node_gallery_item' => 0,
      'found_pet' => 0,
      'pet_video' => 0,
    ),
    'context_admin_vbo_views_bulk_published' => 'both',
  );
  $page->default_handlers[$handler->name] = $handler;
  $pages['lost_pet_manage'] = $page;

  $page = new stdClass();
  $page->disabled = FALSE; /* Edit this to true to make a default page disabled initially */
  $page->api_version = 1;
  $page->name = 'lost_pet_node_add';
  $page->task = 'page';
  $page->admin_title = 'Lost pet node add';
  $page->admin_description = '';
  $page->path = 'lost-pet/add';
  $page->access = array(
    'plugins' => array(
      0 => array(
        'name' => 'perm',
        'settings' => array(
          'perm' => 'create lost_pet content',
        ),
        'context' => 'logged-in-user',
        'not' => FALSE,
      ),
    ),
    'logic' => 'and',
    'type' => 'none',
    'settings' => NULL,
  );
  $page->menu = array(
    'type' => 'action',
    'title' => 'Report Lost or Missing Pet',
    'name' => 'navigation',
    'weight' => '0',
    'parent' => array(
      'type' => 'none',
      'title' => '',
      'name' => 'navigation',
      'weight' => '0',
    ),
  );
  $page->arguments = array();
  $page->conf = array(
    'admin_paths' => TRUE,
  );
  $page->default_handlers = array();
  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'page_lost_pet_node_add_menu_context';
  $handler->task = 'page';
  $handler->subtask = 'lost_pet_node_add';
  $handler->handler = 'menu_context';
  $handler->weight = 0;
  $handler->conf = array(
    'title' => 'Context Admin',
    'no_blocks' => FALSE,
    'css_id' => '',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
    'context_admin_options' => 'node_create_menu',
    'context_admin_options_items' => 'lost_pet',
    'submitted_context' => 'empty',
    'context_admin_use_node_edit' => 1,
  );
  $page->default_handlers[$handler->name] = $handler;
  $pages['lost_pet_node_add'] = $page;

  $page = new stdClass();
  $page->disabled = FALSE; /* Edit this to true to make a default page disabled initially */
  $page->api_version = 1;
  $page->name = 'lost_pet_view';
  $page->task = 'page';
  $page->admin_title = 'Lost pet view';
  $page->admin_description = '';
  $page->path = 'lost-pet/view';
  $page->access = array(
    'logic' => 'and',
  );
  $page->menu = array(
    'type' => 'default tab',
    'title' => 'View',
    'name' => 'navigation',
    'weight' => '0',
    'parent' => array(
      'type' => 'none',
      'title' => '',
      'name' => 'navigation',
      'weight' => '0',
    ),
  );
  $page->arguments = array();
  $page->conf = array(
    'admin_paths' => FALSE,
  );
  $page->default_handlers = array();
  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'page_lost_pet_view_http_response';
  $handler->task = 'page';
  $handler->subtask = 'lost_pet_view';
  $handler->handler = 'http_response';
  $handler->weight = 0;
  $handler->conf = array(
    'title' => 'HTTP response code',
    'contexts' => array(),
    'relationships' => array(),
    'code' => '301',
    'destination' => 'lost-pet',
  );
  $page->default_handlers[$handler->name] = $handler;
  $pages['lost_pet_view'] = $page;

  return $pages;

}
