<?php
/**
 * @file
 * lost_pet.rules_defaults.inc
 */

/**
 * Implements hook_default_rules_configuration().
 */
function lost_pet_default_rules_configuration() {
  $items = array();
  $items['rules_found_flag_reveals_found_message'] = entity_import('rules_config', '{ "rules_found_flag_reveals_found_message" : {
      "LABEL" : "Found flag reveals Found message",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "REQUIRES" : [ "rules", "flag" ],
      "ON" : { "flag_flagged_pet_found" : [] },
      "IF" : [
        { "entity_has_field" : { "entity" : [ "flagged-node" ], "field" : "field_lost_pet_found" } }
      ],
      "DO" : [
        { "data_set" : {
            "data" : [ "flagged-node:field-lost-pet-found" ],
            "value" : "FOUND! FOUND! FOUND!"
          }
        }
      ]
    }
  }');
  $items['rules_found_un_flag_shows_blank_message'] = entity_import('rules_config', '{ "rules_found_un_flag_shows_blank_message" : {
      "LABEL" : "Found un-flag shows blank message",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "REQUIRES" : [ "rules", "flag" ],
      "ON" : { "flag_unflagged_pet_found" : [] },
      "IF" : [
        { "entity_has_field" : { "entity" : [ "flagged-node" ], "field" : "field_lost_pet_found" } }
      ],
      "DO" : [ { "data_set" : { "data" : [ "flagged-node:field-lost-pet-found" ] } } ]
    }
  }');
  return $items;
}
