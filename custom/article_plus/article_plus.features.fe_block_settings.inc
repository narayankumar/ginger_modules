<?php
/**
 * @file
 * article_plus.features.fe_block_settings.inc
 */

/**
 * Implements hook_default_fe_block_settings().
 */
function article_plus_default_fe_block_settings() {
  $export = array();

  $export['version'] = '2.0';

  $export['views-article_teasers_page-block_1'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => 'article_teasers_page-block_1',
    'module' => 'views',
    'node_types' => array(),
    'pages' => '<front>
articles/*',
    'roles' => array(),
    'themes' => array(
      'bartik' => array(
        'region' => 'sidebar_second',
        'status' => 1,
        'theme' => 'bartik',
        'weight' => -13,
      ),
      'seven' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'seven',
        'weight' => 0,
      ),
    ),
    'title' => 'Recent Articles',
    'visibility' => 1,
  );

  $export['views-recent_article_comments-block'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => 'recent_article_comments-block',
    'module' => 'views',
    'node_types' => array(),
    'pages' => 'articles
articles/*',
    'roles' => array(),
    'themes' => array(
      'bartik' => array(
        'region' => 'sidebar_second',
        'status' => 1,
        'theme' => 'bartik',
        'weight' => -12,
      ),
      'seven' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'seven',
        'weight' => 0,
      ),
    ),
    'title' => 'Recent comments on Articles',
    'visibility' => 1,
  );

  return $export;
}
