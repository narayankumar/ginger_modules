<?php
/**
 * @file
 * article_plus.context.inc
 */

/**
 * Implements hook_context_default_contexts().
 */
function article_plus_context_default_contexts() {
  $export = array();

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'articles_blocks';
  $context->description = 'Blocks displaying Articles on node pages plus front page';
  $context->tag = '';
  $context->conditions = array(
    'node' => array(
      'values' => array(
        'article' => 'article',
      ),
      'options' => array(
        'node_form' => '0',
      ),
    ),
    'views' => array(
      'values' => array(
        'article_teasers_page' => 'article_teasers_page',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'views-ad_block_nodequeues-block_7' => array(
          'module' => 'views',
          'delta' => 'ad_block_nodequeues-block_7',
          'region' => 'sidebar_second',
          'weight' => '-10',
        ),
        'views-article_teasers_page-block_1' => array(
          'module' => 'views',
          'delta' => 'article_teasers_page-block_1',
          'region' => 'sidebar_second',
          'weight' => '-9',
        ),
        'views-recent_article_comments-block' => array(
          'module' => 'views',
          'delta' => 'recent_article_comments-block',
          'region' => 'sidebar_second',
          'weight' => '-8',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Blocks displaying Articles on node pages plus front page');
  $export['articles_blocks'] = $context;

  return $export;
}
