<?php
/**
 * @file
 * article_plus.features.uuid_term.inc
 */

/**
 * Implements hook_uuid_features_default_terms().
 */
function article_plus_uuid_features_default_terms() {
  $terms = array();

  $terms[] = array(
    'name' => 'Expert Talk',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 0,
    'uuid' => '2ec167db-f583-41a6-8872-abdfb060fc79',
    'vocabulary_machine_name' => 'voices_category',
  );
  $terms[] = array(
    'name' => 'Opinion',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 0,
    'uuid' => '380d7f78-b4dc-4e41-8004-2b1297f88fb4',
    'vocabulary_machine_name' => 'voices_category',
  );
  $terms[] = array(
    'name' => 'Editorial',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 0,
    'uuid' => '3ad35b24-4299-446e-ab76-923a3b47cba0',
    'vocabulary_machine_name' => 'voices_category',
  );
  $terms[] = array(
    'name' => 'Training',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 1,
    'uuid' => '418a51c4-b884-4dfe-a7e4-f93fa58cc887',
    'vocabulary_machine_name' => 'article_categories',
  );
  $terms[] = array(
    'name' => 'Articles',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 1,
    'uuid' => '717a3406-1da8-449c-9ccf-1e01e5428226',
    'vocabulary_machine_name' => 'magazine_category',
  );
  $terms[] = array(
    'name' => 'Health',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 0,
    'uuid' => '75987c5a-b523-4e4e-9d2d-c1d30353483f',
    'vocabulary_machine_name' => 'article_categories',
  );
  $terms[] = array(
    'name' => 'Voices',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 0,
    'uuid' => 'a4959afc-cdec-4f57-982c-355703757928',
    'vocabulary_machine_name' => 'magazine_category',
  );
  $terms[] = array(
    'name' => 'Grooming',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 2,
    'uuid' => 'f3b55e56-6787-45d7-89b3-78c49fe32c6d',
    'vocabulary_machine_name' => 'article_categories',
  );
  return $terms;
}
