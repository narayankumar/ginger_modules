<?php
/**
 * @file
 * pet_videos.context.inc
 */

/**
 * Implements hook_context_default_contexts().
 */
function pet_videos_context_default_contexts() {
  $export = array();

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'pet_video_node_blocks';
  $context->description = 'Blocks on a single pet video page';
  $context->tag = '';
  $context->conditions = array(
    'node' => array(
      'values' => array(
        'pet_video' => 'pet_video',
      ),
      'options' => array(
        'node_form' => '1',
      ),
    ),
    'views' => array(
      'values' => array(
        'pet_videos_teasers_page' => 'pet_videos_teasers_page',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'views-ad_block_nodequeues-block_2' => array(
          'module' => 'views',
          'delta' => 'ad_block_nodequeues-block_2',
          'region' => 'sidebar_second',
          'weight' => '-10',
        ),
        'views-pet_videos_teasers_page-block_1' => array(
          'module' => 'views',
          'delta' => 'pet_videos_teasers_page-block_1',
          'region' => 'sidebar_second',
          'weight' => '-9',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Blocks on a single pet video page');
  $export['pet_video_node_blocks'] = $context;

  return $export;
}
