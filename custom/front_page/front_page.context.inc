<?php
/**
 * @file
 * front_page.context.inc
 */

/**
 * Implements hook_context_default_contexts().
 */
function front_page_context_default_contexts() {
  $export = array();

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'front_page_blocks';
  $context->description = 'Blocks that appear on front page';
  $context->tag = '';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        '<front>' => '<front>',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'views-nodequeue_1-block' => array(
          'module' => 'views',
          'delta' => 'nodequeue_1-block',
          'region' => 'sidebar_first',
          'weight' => '-8',
        ),
        'views-nodequeue_2-block' => array(
          'module' => 'views',
          'delta' => 'nodequeue_2-block',
          'region' => 'sidebar_first',
          'weight' => '-7',
        ),
        'views-ad_block_nodequeues-block_9' => array(
          'module' => 'views',
          'delta' => 'ad_block_nodequeues-block_9',
          'region' => 'sidebar_second',
          'weight' => '-9',
        ),
        'views-9dd9c477a269547d1ec07a2f78299250' => array(
          'module' => 'views',
          'delta' => '9dd9c477a269547d1ec07a2f78299250',
          'region' => 'sidebar_second',
          'weight' => '-8',
        ),
        'views-nodequeue_3-block' => array(
          'module' => 'views',
          'delta' => 'nodequeue_3-block',
          'region' => 'sidebar_second',
          'weight' => '-7',
        ),
        'views-comments_recent-block' => array(
          'module' => 'views',
          'delta' => 'comments_recent-block',
          'region' => 'sidebar_second',
          'weight' => '-6',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Blocks that appear on front page');
  $export['front_page_blocks'] = $context;

  return $export;
}
