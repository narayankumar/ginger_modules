<?php
/**
 * @file
 * directory.features.uuid_term.inc
 */

/**
 * Implements hook_uuid_features_default_terms().
 */
function directory_uuid_features_default_terms() {
  $terms = array();

  $terms[] = array(
    'name' => 'Vet',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 0,
    'uuid' => '08357813-b1fa-4f16-85c2-61453904cdd4',
    'vocabulary_machine_name' => 'directory_category',
  );
  $terms[] = array(
    'name' => 'Pet Lodging',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 0,
    'uuid' => '5312f781-8e4e-42df-a985-890f2666329b',
    'vocabulary_machine_name' => 'directory_category',
  );
  $terms[] = array(
    'name' => 'Pet Shop',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 0,
    'uuid' => '5816a295-9e47-477b-8299-f63b0ac61b34',
    'vocabulary_machine_name' => 'directory_category',
  );
  $terms[] = array(
    'name' => 'Pet Grooming',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 0,
    'uuid' => '7f2299b5-c0c6-4f21-addb-bc013d2bd393',
    'vocabulary_machine_name' => 'directory_category',
  );
  $terms[] = array(
    'name' => 'Pet Training',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 0,
    'uuid' => 'da0039e4-f96a-4fd0-bc95-23e4f20c99d2',
    'vocabulary_machine_name' => 'directory_category',
  );
  $terms[] = array(
    'name' => 'Online shops and services',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 0,
    'uuid' => 'db72bda1-0b37-4596-a752-bb4988ad227b',
    'vocabulary_machine_name' => 'directory_category',
  );
  $terms[] = array(
    'name' => 'Aquarium',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 0,
    'uuid' => 'e72c059f-1385-4041-8a9c-381331e9bd0c',
    'vocabulary_machine_name' => 'directory_category',
  );
  return $terms;
}
