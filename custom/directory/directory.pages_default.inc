<?php
/**
 * @file
 * directory.pages_default.inc
 */

/**
 * Implements hook_default_page_manager_pages().
 */
function directory_default_page_manager_pages() {
  $page = new stdClass();
  $page->disabled = FALSE; /* Edit this to true to make a default page disabled initially */
  $page->api_version = 1;
  $page->name = 'directory_category_add_tab';
  $page->task = 'page';
  $page->admin_title = 'Directory category add tab';
  $page->admin_description = 'Used for adding terms to Directory Category taxonomy list for Directory CT';
  $page->path = 'directory/category-add';
  $page->access = array(
    'plugins' => array(
      0 => array(
        'name' => 'perm',
        'settings' => array(
          'perm' => 'edit any directory_entry content',
        ),
        'context' => 'logged-in-user',
        'not' => FALSE,
      ),
    ),
    'logic' => 'and',
    'type' => 'none',
    'settings' => NULL,
  );
  $page->menu = array(
    'type' => 'tab',
    'title' => 'Add category',
    'name' => 'navigation',
    'weight' => '3',
    'parent' => array(
      'type' => 'tab',
      'title' => 'City',
      'name' => 'navigation',
      'weight' => '0',
    ),
  );
  $page->arguments = array();
  $page->conf = array(
    'admin_paths' => FALSE,
  );
  $page->default_handlers = array();
  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'page_directory_category_add_tab_menu_context';
  $handler->task = 'page';
  $handler->subtask = 'directory_category_add_tab';
  $handler->handler = 'menu_context';
  $handler->weight = 0;
  $handler->conf = array(
    'title' => 'Context Admin',
    'no_blocks' => FALSE,
    'css_id' => '',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
    'context_admin_options' => 'taxonomy_list_menu',
    'context_admin_vocabulary' => 'directory_category',
    'context_admin_vocabulary_options' => 'add',
  );
  $page->default_handlers[$handler->name] = $handler;
  $pages['directory_category_add_tab'] = $page;

  $page = new stdClass();
  $page->disabled = FALSE; /* Edit this to true to make a default page disabled initially */
  $page->api_version = 1;
  $page->name = 'directory_category_manage_tab';
  $page->task = 'page';
  $page->admin_title = 'Directory category manage tab';
  $page->admin_description = 'Used for managing Directory category taxonomy list for Directory CT';
  $page->path = 'directory/category';
  $page->access = array(
    'plugins' => array(
      0 => array(
        'name' => 'perm',
        'settings' => array(
          'perm' => 'edit any directory_entry content',
        ),
        'context' => 'logged-in-user',
        'not' => FALSE,
      ),
    ),
    'logic' => 'and',
    'type' => 'none',
    'settings' => NULL,
  );
  $page->menu = array(
    'type' => 'tab',
    'title' => 'List categories',
    'name' => 'navigation',
    'weight' => '2',
    'parent' => array(
      'type' => 'tab',
      'title' => 'City',
      'name' => 'navigation',
      'weight' => '0',
    ),
  );
  $page->arguments = array();
  $page->conf = array(
    'admin_paths' => FALSE,
  );
  $page->default_handlers = array();
  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'page_directory_category_manage_tab_menu_context';
  $handler->task = 'page';
  $handler->subtask = 'directory_category_manage_tab';
  $handler->handler = 'menu_context';
  $handler->weight = 0;
  $handler->conf = array(
    'title' => 'Context Admin',
    'no_blocks' => FALSE,
    'css_id' => '',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
    'context_admin_options' => 'taxonomy_list_menu',
    'context_admin_vocabulary' => 'directory_category',
    'context_admin_vocabulary_options' => 'list',
  );
  $page->default_handlers[$handler->name] = $handler;
  $pages['directory_category_manage_tab'] = $page;

  $page = new stdClass();
  $page->disabled = FALSE; /* Edit this to true to make a default page disabled initially */
  $page->api_version = 1;
  $page->name = 'directory_manage';
  $page->task = 'page';
  $page->admin_title = 'Directory manage';
  $page->admin_description = '';
  $page->path = 'directory/manage';
  $page->access = array(
    'plugins' => array(
      0 => array(
        'name' => 'perm',
        'settings' => array(
          'perm' => 'edit any directory_entry content',
        ),
        'context' => 'logged-in-user',
        'not' => FALSE,
      ),
    ),
    'logic' => 'and',
    'type' => 'none',
    'settings' => NULL,
  );
  $page->menu = array(
    'type' => 'tab',
    'title' => 'Manage',
    'name' => 'navigation',
    'weight' => '1',
    'parent' => array(
      'type' => 'none',
      'title' => '',
      'name' => 'navigation',
      'weight' => '0',
    ),
  );
  $page->arguments = array();
  $page->conf = array(
    'admin_paths' => TRUE,
  );
  $page->default_handlers = array();
  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'page_directory_manage_menu_context';
  $handler->task = 'page';
  $handler->subtask = 'directory_manage';
  $handler->handler = 'menu_context';
  $handler->weight = 0;
  $handler->conf = array(
    'title' => 'Context Admin',
    'no_blocks' => FALSE,
    'css_id' => '',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
    'context_admin_options' => 'views_bulk_menu',
    'context_admin_vbo_machine_name' => 'directory_manage',
    'context_admin_vbo_views_bulk_node_type' => array(
      'directory_entry' => 'directory_entry',
      'article' => 0,
      'lost_pet' => 0,
      'found_pet' => 0,
      'pet_for_adoption' => 0,
      'page' => 0,
      'news' => 0,
      'guest_column' => 0,
      'announcement' => 0,
      'node_gallery_gallery' => 0,
      'node_gallery_item' => 0,
      'pet_video' => 0,
    ),
    'context_admin_vbo_views_bulk_published' => 'both',
  );
  $page->default_handlers[$handler->name] = $handler;
  $pages['directory_manage'] = $page;

  $page = new stdClass();
  $page->disabled = FALSE; /* Edit this to true to make a default page disabled initially */
  $page->api_version = 1;
  $page->name = 'directory_node_add';
  $page->task = 'page';
  $page->admin_title = 'Directory node add';
  $page->admin_description = '';
  $page->path = 'directory/add';
  $page->access = array(
    'plugins' => array(
      0 => array(
        'name' => 'perm',
        'settings' => array(
          'perm' => 'create directory_entry content',
        ),
        'context' => 'logged-in-user',
        'not' => FALSE,
      ),
    ),
    'logic' => 'and',
    'type' => 'none',
    'settings' => NULL,
  );
  $page->menu = array(
    'type' => 'action',
    'title' => 'Create Directory Entry',
    'name' => 'navigation',
    'weight' => '0',
    'parent' => array(
      'type' => 'none',
      'title' => '',
      'name' => 'navigation',
      'weight' => '0',
    ),
  );
  $page->arguments = array();
  $page->conf = array(
    'admin_paths' => TRUE,
  );
  $page->default_handlers = array();
  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'page_directory_node_add_menu_context';
  $handler->task = 'page';
  $handler->subtask = 'directory_node_add';
  $handler->handler = 'menu_context';
  $handler->weight = 0;
  $handler->conf = array(
    'title' => 'Context Admin',
    'no_blocks' => FALSE,
    'css_id' => '',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
    'context_admin_options' => 'node_create_menu',
    'context_admin_options_items' => 'directory_entry',
    'submitted_context' => 'empty',
    'context_admin_use_node_edit' => 1,
  );
  $page->default_handlers[$handler->name] = $handler;
  $pages['directory_node_add'] = $page;

  $page = new stdClass();
  $page->disabled = FALSE; /* Edit this to true to make a default page disabled initially */
  $page->api_version = 1;
  $page->name = 'directory_node_view';
  $page->task = 'page';
  $page->admin_title = 'Directory node view';
  $page->admin_description = '';
  $page->path = 'directory/view';
  $page->access = array(
    'type' => 'none',
    'settings' => NULL,
  );
  $page->menu = array(
    'type' => 'default tab',
    'title' => 'View',
    'name' => 'navigation',
    'weight' => '0',
    'parent' => array(
      'type' => 'none',
      'title' => '',
      'name' => 'navigation',
      'weight' => '0',
    ),
  );
  $page->arguments = array();
  $page->conf = array(
    'admin_paths' => FALSE,
  );
  $page->default_handlers = array();
  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'page_directory_node_view_http_response';
  $handler->task = 'page';
  $handler->subtask = 'directory_node_view';
  $handler->handler = 'http_response';
  $handler->weight = 0;
  $handler->conf = array(
    'title' => 'HTTP response code',
    'contexts' => array(),
    'relationships' => array(),
    'code' => '301',
    'destination' => 'directory',
  );
  $page->default_handlers[$handler->name] = $handler;
  $pages['directory_node_view'] = $page;

  return $pages;

}
