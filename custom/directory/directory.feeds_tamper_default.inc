<?php
/**
 * @file
 * directory.feeds_tamper_default.inc
 */

/**
 * Implements hook_feeds_tamper_default().
 */
function directory_feeds_tamper_default() {
  $export = array();

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'directory_importer-category-explode';
  $feeds_tamper->importer = 'directory_importer';
  $feeds_tamper->source = 'Category';
  $feeds_tamper->plugin_id = 'explode';
  $feeds_tamper->settings = array(
    'separator' => ',',
    'limit' => '',
    'real_separator' => ',',
  );
  $feeds_tamper->weight = 0;
  $feeds_tamper->description = 'Explode directory category items';
  $export['directory_importer-category-explode'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'directory_importer-directory_category-explode';
  $feeds_tamper->importer = 'directory_importer';
  $feeds_tamper->source = 'Directory category';
  $feeds_tamper->plugin_id = 'explode';
  $feeds_tamper->settings = array(
    'separator' => ',',
    'limit' => '',
    'real_separator' => ',',
  );
  $feeds_tamper->weight = 0;
  $feeds_tamper->description = 'Explode directory category items';
  $export['directory_importer-directory_category-explode'] = $feeds_tamper;

  return $export;
}
