<?php
/**
 * @file
 * directory.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function directory_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'administer feeds'.
  $permissions['administer feeds'] = array(
    'name' => 'administer feeds',
    'roles' => array(
      'administrator' => 'administrator',
      'site admin' => 'site admin',
    ),
    'module' => 'feeds',
  );

  // Exported permission: 'clear directory_importer feeds'.
  $permissions['clear directory_importer feeds'] = array(
    'name' => 'clear directory_importer feeds',
    'roles' => array(
      'administrator' => 'administrator',
      'editor' => 'editor',
      'site admin' => 'site admin',
    ),
    'module' => 'feeds',
  );

  // Exported permission: 'create directory_entry content'.
  $permissions['create directory_entry content'] = array(
    'name' => 'create directory_entry content',
    'roles' => array(
      'administrator' => 'administrator',
      'directory user' => 'directory user',
      'editor' => 'editor',
      'site admin' => 'site admin',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete any directory_entry content'.
  $permissions['delete any directory_entry content'] = array(
    'name' => 'delete any directory_entry content',
    'roles' => array(
      'administrator' => 'administrator',
      'editor' => 'editor',
      'site admin' => 'site admin',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete own directory_entry content'.
  $permissions['delete own directory_entry content'] = array(
    'name' => 'delete own directory_entry content',
    'roles' => array(
      'administrator' => 'administrator',
      'editor' => 'editor',
      'site admin' => 'site admin',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete terms in directory_category'.
  $permissions['delete terms in directory_category'] = array(
    'name' => 'delete terms in directory_category',
    'roles' => array(
      'administrator' => 'administrator',
      'editor' => 'editor',
      'site admin' => 'site admin',
    ),
    'module' => 'taxonomy',
  );

  // Exported permission: 'edit any directory_entry content'.
  $permissions['edit any directory_entry content'] = array(
    'name' => 'edit any directory_entry content',
    'roles' => array(
      'administrator' => 'administrator',
      'editor' => 'editor',
      'site admin' => 'site admin',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit own directory_entry content'.
  $permissions['edit own directory_entry content'] = array(
    'name' => 'edit own directory_entry content',
    'roles' => array(
      'administrator' => 'administrator',
      'directory user' => 'directory user',
      'editor' => 'editor',
      'site admin' => 'site admin',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit terms in directory_category'.
  $permissions['edit terms in directory_category'] = array(
    'name' => 'edit terms in directory_category',
    'roles' => array(
      'administrator' => 'administrator',
      'editor' => 'editor',
      'site admin' => 'site admin',
    ),
    'module' => 'taxonomy',
  );

  // Exported permission: 'flag anon_flag_for_incorrect_info'.
  $permissions['flag anon_flag_for_incorrect_info'] = array(
    'name' => 'flag anon_flag_for_incorrect_info',
    'roles' => array(
      'anonymous user' => 'anonymous user',
    ),
    'module' => 'flag',
  );

  // Exported permission: 'flag authenticated_flag_for_incorrect'.
  $permissions['flag authenticated_flag_for_incorrect'] = array(
    'name' => 'flag authenticated_flag_for_incorrect',
    'roles' => array(
      'administrator' => 'administrator',
      'directory user' => 'directory user',
      'editor' => 'editor',
      'forum monitor' => 'forum monitor',
      'registered user' => 'registered user',
      'retailer' => 'retailer',
      'site admin' => 'site admin',
      'writer' => 'writer',
    ),
    'module' => 'flag',
  );

  // Exported permission: 'flag im_owner_for_anon'.
  $permissions['flag im_owner_for_anon'] = array(
    'name' => 'flag im_owner_for_anon',
    'roles' => array(
      'anonymous user' => 'anonymous user',
    ),
    'module' => 'flag',
  );

  // Exported permission: 'flag im_owner_for_registered'.
  $permissions['flag im_owner_for_registered'] = array(
    'name' => 'flag im_owner_for_registered',
    'roles' => array(
      'administrator' => 'administrator',
      'directory user' => 'directory user',
      'editor' => 'editor',
      'forum monitor' => 'forum monitor',
      'registered user' => 'registered user',
      'retailer' => 'retailer',
      'site admin' => 'site admin',
      'writer' => 'writer',
    ),
    'module' => 'flag',
  );

  // Exported permission: 'import directory_importer feeds'.
  $permissions['import directory_importer feeds'] = array(
    'name' => 'import directory_importer feeds',
    'roles' => array(
      'administrator' => 'administrator',
      'editor' => 'editor',
      'site admin' => 'site admin',
    ),
    'module' => 'feeds',
  );

  // Exported permission: 'publish button publish any directory_entry'.
  $permissions['publish button publish any directory_entry'] = array(
    'name' => 'publish button publish any directory_entry',
    'roles' => array(
      'administrator' => 'administrator',
      'editor' => 'editor',
      'site admin' => 'site admin',
    ),
    'module' => 'publish_button',
  );

  // Exported permission: 'publish button publish editable directory_entry'.
  $permissions['publish button publish editable directory_entry'] = array(
    'name' => 'publish button publish editable directory_entry',
    'roles' => array(
      'administrator' => 'administrator',
      'editor' => 'editor',
      'site admin' => 'site admin',
    ),
    'module' => 'publish_button',
  );

  // Exported permission: 'publish button publish own directory_entry'.
  $permissions['publish button publish own directory_entry'] = array(
    'name' => 'publish button publish own directory_entry',
    'roles' => array(
      'administrator' => 'administrator',
      'editor' => 'editor',
      'site admin' => 'site admin',
    ),
    'module' => 'publish_button',
  );

  // Exported permission: 'publish button unpublish any directory_entry'.
  $permissions['publish button unpublish any directory_entry'] = array(
    'name' => 'publish button unpublish any directory_entry',
    'roles' => array(
      'administrator' => 'administrator',
      'editor' => 'editor',
      'site admin' => 'site admin',
    ),
    'module' => 'publish_button',
  );

  // Exported permission: 'publish button unpublish editable directory_entry'.
  $permissions['publish button unpublish editable directory_entry'] = array(
    'name' => 'publish button unpublish editable directory_entry',
    'roles' => array(
      'administrator' => 'administrator',
      'editor' => 'editor',
      'site admin' => 'site admin',
    ),
    'module' => 'publish_button',
  );

  // Exported permission: 'publish button unpublish own directory_entry'.
  $permissions['publish button unpublish own directory_entry'] = array(
    'name' => 'publish button unpublish own directory_entry',
    'roles' => array(
      'administrator' => 'administrator',
      'editor' => 'editor',
      'site admin' => 'site admin',
    ),
    'module' => 'publish_button',
  );

  // Exported permission: 'unflag anon_flag_for_incorrect_info'.
  $permissions['unflag anon_flag_for_incorrect_info'] = array(
    'name' => 'unflag anon_flag_for_incorrect_info',
    'roles' => array(),
    'module' => 'flag',
  );

  // Exported permission: 'unflag authenticated_flag_for_incorrect'.
  $permissions['unflag authenticated_flag_for_incorrect'] = array(
    'name' => 'unflag authenticated_flag_for_incorrect',
    'roles' => array(
      'administrator' => 'administrator',
      'directory user' => 'directory user',
      'editor' => 'editor',
      'forum monitor' => 'forum monitor',
      'registered user' => 'registered user',
      'retailer' => 'retailer',
      'site admin' => 'site admin',
      'writer' => 'writer',
    ),
    'module' => 'flag',
  );

  // Exported permission: 'unflag im_owner_for_anon'.
  $permissions['unflag im_owner_for_anon'] = array(
    'name' => 'unflag im_owner_for_anon',
    'roles' => array(),
    'module' => 'flag',
  );

  // Exported permission: 'unflag im_owner_for_registered'.
  $permissions['unflag im_owner_for_registered'] = array(
    'name' => 'unflag im_owner_for_registered',
    'roles' => array(
      'administrator' => 'administrator',
      'site admin' => 'site admin',
    ),
    'module' => 'flag',
  );

  // Exported permission: 'unlock directory_importer feeds'.
  $permissions['unlock directory_importer feeds'] = array(
    'name' => 'unlock directory_importer feeds',
    'roles' => array(
      'administrator' => 'administrator',
      'site admin' => 'site admin',
    ),
    'module' => 'feeds',
  );

  return $permissions;
}
