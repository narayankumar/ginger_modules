<?php
/**
 * @file
 * user_tabs.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function user_tabs_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'access site-wide contact form'.
  $permissions['access site-wide contact form'] = array(
    'name' => 'access site-wide contact form',
    'roles' => array(
      'administrator' => 'administrator',
      'anonymous user' => 'anonymous user',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'contact',
  );

  // Exported permission: 'access user contact forms'.
  $permissions['access user contact forms'] = array(
    'name' => 'access user contact forms',
    'roles' => array(
      'administrator' => 'administrator',
      'site admin' => 'site admin',
    ),
    'module' => 'contact',
  );

  // Exported permission: 'administer Statuses settings'.
  $permissions['administer Statuses settings'] = array(
    'name' => 'administer Statuses settings',
    'roles' => array(
      'administrator' => 'administrator',
      'site admin' => 'site admin',
    ),
    'module' => 'statuses',
  );

  // Exported permission: 'administer Terms and Conditions'.
  $permissions['administer Terms and Conditions'] = array(
    'name' => 'administer Terms and Conditions',
    'roles' => array(
      'administrator' => 'administrator',
      'site admin' => 'site admin',
    ),
    'module' => 'legal',
  );

  // Exported permission: 'administer contact forms'.
  $permissions['administer contact forms'] = array(
    'name' => 'administer contact forms',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'contact',
  );

  // Exported permission: 'delete all status comments'.
  $permissions['delete all status comments'] = array(
    'name' => 'delete all status comments',
    'roles' => array(
      'administrator' => 'administrator',
      'site admin' => 'site admin',
    ),
    'module' => 'fbss_comments',
  );

  // Exported permission: 'delete all statuses'.
  $permissions['delete all statuses'] = array(
    'name' => 'delete all statuses',
    'roles' => array(
      'administrator' => 'administrator',
      'site admin' => 'site admin',
    ),
    'module' => 'statuses',
  );

  // Exported permission: 'delete comments on own statuses'.
  $permissions['delete comments on own statuses'] = array(
    'name' => 'delete comments on own statuses',
    'roles' => array(
      'administrator' => 'administrator',
      'directory user' => 'directory user',
      'editor' => 'editor',
      'forum monitor' => 'forum monitor',
      'registered user' => 'registered user',
      'retailer' => 'retailer',
      'site admin' => 'site admin',
      'writer' => 'writer',
    ),
    'module' => 'fbss_comments',
  );

  // Exported permission: 'delete own status comments'.
  $permissions['delete own status comments'] = array(
    'name' => 'delete own status comments',
    'roles' => array(
      'administrator' => 'administrator',
      'directory user' => 'directory user',
      'editor' => 'editor',
      'forum monitor' => 'forum monitor',
      'registered user' => 'registered user',
      'retailer' => 'retailer',
      'site admin' => 'site admin',
      'writer' => 'writer',
    ),
    'module' => 'fbss_comments',
  );

  // Exported permission: 'delete own statuses'.
  $permissions['delete own statuses'] = array(
    'name' => 'delete own statuses',
    'roles' => array(
      'administrator' => 'administrator',
      'directory user' => 'directory user',
      'editor' => 'editor',
      'forum monitor' => 'forum monitor',
      'registered user' => 'registered user',
      'retailer' => 'retailer',
      'site admin' => 'site admin',
      'writer' => 'writer',
    ),
    'module' => 'statuses',
  );

  // Exported permission: 'delete status messages on own nodes'.
  $permissions['delete status messages on own nodes'] = array(
    'name' => 'delete status messages on own nodes',
    'roles' => array(
      'administrator' => 'administrator',
      'directory user' => 'directory user',
      'editor' => 'editor',
      'forum monitor' => 'forum monitor',
      'registered user' => 'registered user',
      'retailer' => 'retailer',
      'site admin' => 'site admin',
      'writer' => 'writer',
    ),
    'module' => 'statuses',
  );

  // Exported permission: 'delete status messages on own profile'.
  $permissions['delete status messages on own profile'] = array(
    'name' => 'delete status messages on own profile',
    'roles' => array(
      'administrator' => 'administrator',
      'directory user' => 'directory user',
      'editor' => 'editor',
      'forum monitor' => 'forum monitor',
      'registered user' => 'registered user',
      'retailer' => 'retailer',
      'site admin' => 'site admin',
      'writer' => 'writer',
    ),
    'module' => 'statuses',
  );

  // Exported permission: 'edit all status comments'.
  $permissions['edit all status comments'] = array(
    'name' => 'edit all status comments',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'fbss_comments',
  );

  // Exported permission: 'edit all statuses'.
  $permissions['edit all statuses'] = array(
    'name' => 'edit all statuses',
    'roles' => array(
      'administrator' => 'administrator',
      'site admin' => 'site admin',
    ),
    'module' => 'statuses',
  );

  // Exported permission: 'edit comments on own statuses'.
  $permissions['edit comments on own statuses'] = array(
    'name' => 'edit comments on own statuses',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'fbss_comments',
  );

  // Exported permission: 'edit own status comments'.
  $permissions['edit own status comments'] = array(
    'name' => 'edit own status comments',
    'roles' => array(
      'administrator' => 'administrator',
      'directory user' => 'directory user',
      'editor' => 'editor',
      'forum monitor' => 'forum monitor',
      'registered user' => 'registered user',
      'retailer' => 'retailer',
      'site admin' => 'site admin',
      'writer' => 'writer',
    ),
    'module' => 'fbss_comments',
  );

  // Exported permission: 'edit own statuses'.
  $permissions['edit own statuses'] = array(
    'name' => 'edit own statuses',
    'roles' => array(
      'administrator' => 'administrator',
      'directory user' => 'directory user',
      'editor' => 'editor',
      'forum monitor' => 'forum monitor',
      'registered user' => 'registered user',
      'retailer' => 'retailer',
      'site admin' => 'site admin',
      'writer' => 'writer',
    ),
    'module' => 'statuses',
  );

  // Exported permission: 'flag abusive_user_flag'.
  $permissions['flag abusive_user_flag'] = array(
    'name' => 'flag abusive_user_flag',
    'roles' => array(
      'administrator' => 'administrator',
      'authenticated user' => 'authenticated user',
      'directory user' => 'directory user',
      'editor' => 'editor',
      'forum monitor' => 'forum monitor',
      'registered user' => 'registered user',
      'retailer' => 'retailer',
      'site admin' => 'site admin',
      'writer' => 'writer',
    ),
    'module' => 'flag',
  );

  // Exported permission: 'flag follow_user'.
  $permissions['flag follow_user'] = array(
    'name' => 'flag follow_user',
    'roles' => array(
      'administrator' => 'administrator',
      'authenticated user' => 'authenticated user',
      'directory user' => 'directory user',
      'editor' => 'editor',
      'forum monitor' => 'forum monitor',
      'registered user' => 'registered user',
      'retailer' => 'retailer',
      'site admin' => 'site admin',
      'writer' => 'writer',
    ),
    'module' => 'flag',
  );

  // Exported permission: 'post status comment'.
  $permissions['post status comment'] = array(
    'name' => 'post status comment',
    'roles' => array(
      'administrator' => 'administrator',
      'directory user' => 'directory user',
      'editor' => 'editor',
      'forum monitor' => 'forum monitor',
      'registered user' => 'registered user',
      'retailer' => 'retailer',
      'site admin' => 'site admin',
      'writer' => 'writer',
    ),
    'module' => 'fbss_comments',
  );

  // Exported permission: 'post status messages to other streams'.
  $permissions['post status messages to other streams'] = array(
    'name' => 'post status messages to other streams',
    'roles' => array(
      'administrator' => 'administrator',
      'directory user' => 'directory user',
      'editor' => 'editor',
      'forum monitor' => 'forum monitor',
      'registered user' => 'registered user',
      'retailer' => 'retailer',
      'site admin' => 'site admin',
      'writer' => 'writer',
    ),
    'module' => 'statuses',
  );

  // Exported permission: 'send messages to all users at once'.
  $permissions['send messages to all users at once'] = array(
    'name' => 'send messages to all users at once',
    'roles' => array(
      'administrator' => 'administrator',
      'site admin' => 'site admin',
    ),
    'module' => 'statuses',
  );

  // Exported permission: 'send private status messages'.
  $permissions['send private status messages'] = array(
    'name' => 'send private status messages',
    'roles' => array(
      'administrator' => 'administrator',
      'directory user' => 'directory user',
      'editor' => 'editor',
      'forum monitor' => 'forum monitor',
      'registered user' => 'registered user',
      'retailer' => 'retailer',
      'site admin' => 'site admin',
      'writer' => 'writer',
    ),
    'module' => 'fbss_privacy',
  );

  // Exported permission: 'unflag abusive_user_flag'.
  $permissions['unflag abusive_user_flag'] = array(
    'name' => 'unflag abusive_user_flag',
    'roles' => array(
      'administrator' => 'administrator',
      'authenticated user' => 'authenticated user',
      'directory user' => 'directory user',
      'editor' => 'editor',
      'forum monitor' => 'forum monitor',
      'registered user' => 'registered user',
      'retailer' => 'retailer',
      'site admin' => 'site admin',
      'writer' => 'writer',
    ),
    'module' => 'flag',
  );

  // Exported permission: 'unflag follow_user'.
  $permissions['unflag follow_user'] = array(
    'name' => 'unflag follow_user',
    'roles' => array(
      'administrator' => 'administrator',
      'authenticated user' => 'authenticated user',
      'directory user' => 'directory user',
      'editor' => 'editor',
      'forum monitor' => 'forum monitor',
      'registered user' => 'registered user',
      'retailer' => 'retailer',
      'site admin' => 'site admin',
      'writer' => 'writer',
    ),
    'module' => 'flag',
  );

  // Exported permission: 'update and view own stream'.
  $permissions['update and view own stream'] = array(
    'name' => 'update and view own stream',
    'roles' => array(
      'administrator' => 'administrator',
      'directory user' => 'directory user',
      'editor' => 'editor',
      'forum monitor' => 'forum monitor',
      'registered user' => 'registered user',
      'retailer' => 'retailer',
      'site admin' => 'site admin',
      'writer' => 'writer',
    ),
    'module' => 'statuses',
  );

  // Exported permission: 'use PHP for context visibility'.
  $permissions['use PHP for context visibility'] = array(
    'name' => 'use PHP for context visibility',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'statuses',
  );

  // Exported permission: 'view Terms and Conditions'.
  $permissions['view Terms and Conditions'] = array(
    'name' => 'view Terms and Conditions',
    'roles' => array(
      'anonymous user' => 'anonymous user',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'legal',
  );

  // Exported permission: 'view all private status messages'.
  $permissions['view all private status messages'] = array(
    'name' => 'view all private status messages',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'fbss_privacy',
  );

  // Exported permission: 'view all status comments'.
  $permissions['view all status comments'] = array(
    'name' => 'view all status comments',
    'roles' => array(
      'administrator' => 'administrator',
      'directory user' => 'directory user',
      'editor' => 'editor',
      'forum monitor' => 'forum monitor',
      'registered user' => 'registered user',
      'retailer' => 'retailer',
      'site admin' => 'site admin',
      'writer' => 'writer',
    ),
    'module' => 'fbss_comments',
  );

  // Exported permission: 'view all statuses'.
  $permissions['view all statuses'] = array(
    'name' => 'view all statuses',
    'roles' => array(
      'administrator' => 'administrator',
      'directory user' => 'directory user',
      'editor' => 'editor',
      'forum monitor' => 'forum monitor',
      'registered user' => 'registered user',
      'retailer' => 'retailer',
      'site admin' => 'site admin',
      'writer' => 'writer',
    ),
    'module' => 'statuses',
  );

  // Exported permission: 'view own status comments'.
  $permissions['view own status comments'] = array(
    'name' => 'view own status comments',
    'roles' => array(
      'administrator' => 'administrator',
      'directory user' => 'directory user',
      'editor' => 'editor',
      'forum monitor' => 'forum monitor',
      'registered user' => 'registered user',
      'retailer' => 'retailer',
      'site admin' => 'site admin',
      'writer' => 'writer',
    ),
    'module' => 'fbss_comments',
  );

  return $permissions;
}
