Release v0.13 created in test branch - all corrections done - 
MILESTONE 5 COMPLETED - from here, GA, Mollom and FB Connect 
will be installed and configured only on live site

- - - - - 

Release v0.11 created in test branch - all corrections done - 
MILESTONE 4 COMPLETED

- - - - - 

Release v0.9 created in test branch - all corrections done - 
MILESTONE 3 COMPLETED

- - - - - 

Release v0.7 created in test branch - submitted for approval
MILESTONE 3 SUBMITTED

- - - - -

Release v0.6 created in test branch - all corrections done -
MILESTONE 2 COMPLETED

- - - - -

Release v0.5a created in dev and merged to test - added Manage tabs to 
lost and found teasers pages

- - - - -

Release v0.5 created - sent for corrections from nishant - Module 2 is over
subject to corrections/additions

- - - - -

Release v0.4 created - it's basically v0.3 presented, corrected and approved

This completes MILESTONE 1 (ending with issue #18 on bitbucket)

- - - - -

Release 0.3 created in test branch at test.ginger.arbitrandom.com

Includes all development work upto creation of context blocks in place of

drupal blocks system. Front page feature created.

All MILESTONE 1 work completed - CTs are article, news, guest column and announcement.

to be presented to client on 20/04/2014

- - - - -

Release 0.2 created in test branch at test.ginger.arbitrandom.com

this is the release that was shared with client on 15/04
